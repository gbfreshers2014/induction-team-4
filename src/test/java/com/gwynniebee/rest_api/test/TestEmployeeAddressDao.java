/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.rest_api.test;

import java.util.List;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.AddressDetailsObject;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.dao.EmpPerDetaildao;
import com.gwynniebee.dao.EmployeeAddressDao;
import com.gwynniebee.rest_api.test.helper.LiquibaseOperations;
import com.gwynniebee.rest_api.test.helper.TestDAOBase;

/**
 * Base class for DAO Test.
 * @author sumit
 */

public class TestEmployeeAddressDao extends TestDAOBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestEmployeeAddressDao.class);

    @Test
    public void testsqlQueryAddress() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        EmployeeAddressDao e2 = h1.attach(EmployeeAddressDao.class);
        int response_status;
        try {
            String lastUpdateBy = "1234";
            String employeeId = "in1";
            String email = "sgupta@gwynniebee.com";
            String firstName = "sumit";
            String lastName = "gupta";
            String dob = "1991-04-06";
            String doj = "2014-07-01";
            String bloodGroup = "o+";
            String employmentStatus = "regular"; // ENUM('retired','intern','third-party','regular'),
            String designation = "se";

            PersonalDetails details = new PersonalDetails();
            details.setlastUpdateBy(lastUpdateBy);
            details.setEmployeeId(employeeId);
            details.setEmail(email);
            details.setFirstName(firstName);
            details.setLastName(lastName);
            details.setDob(dob);
            details.setDoj(doj);
            details.setBloodGroup(bloodGroup);
            details.setEmploymentStatus(employmentStatus);
            details.setDesignation(designation);

            AddressDetails addr_details1 = new AddressDetails();
            AddressDetails addr_details2 = new AddressDetails();
            String type1 = "present";
            String streetAddress1 = "H.no 1095";
            String city1 = "Kalka";
            String state1 = "Haryana";
            String country1 = "IN";
            String zipcode1 = "133302";
            String createdBy1 = "1234";
            String lastUpdateBy1 = "1234";
            addr_details1.setCity(city1);
            addr_details1.setCountry(country1);
            addr_details1.setCreatedBy(createdBy1);
            addr_details1.setLastUpdateBy(lastUpdateBy1);
            addr_details1.setState(state1);
            addr_details1.setStreetAddress(streetAddress1);
            addr_details1.setType(type1);
            addr_details1.setZipcode(zipcode1);

            String type2 = "present";
            String streetAddress2 = "L-50c";
            String city2 = "Saket";
            String state2 = "New Delhi";
            String country2 = "IN";
            String zipcode2 = "110017";
            String createdBy2 = "1234";
            String lastUpdateBy2 = "1234";

            addr_details2.setCity(city2);
            addr_details2.setCountry(country2);
            addr_details2.setCreatedBy(createdBy2);
            addr_details2.setLastUpdateBy(lastUpdateBy2);
            addr_details2.setState(state2);
            addr_details2.setStreetAddress(streetAddress2);
            addr_details2.setType(type2);
            addr_details2.setZipcode(zipcode2);

            h1.begin();
            e1.addPersoDetails(details, "1234", "1234");
            e1.addAddressDetails("1234", city1, country1, state1, streetAddress1, addr_details1, "1234");
            e1.addAddressDetails("1234", city2, country2, state2, streetAddress2, addr_details2, "1234");
            h1.commit();

            List<AddressDetailsObject> resp_addr = e2.sqlqueryaddress("1234");

            if ((resp_addr == null) || (resp_addr.size() == 0)) {
                assert false;
            }

            for (AddressDetailsObject addr : resp_addr) {
                System.out.println(addr.getCity());
                System.out.println(addr.getState());
            }

        } finally {
            h1.close();
        }
    }
}
