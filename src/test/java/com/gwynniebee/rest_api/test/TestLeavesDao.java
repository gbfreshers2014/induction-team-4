package com.gwynniebee.rest_api.test;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.LeavesDetails;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.dao.AttendanceUploadDao;
import com.gwynniebee.dao.EmpPerDetaildao;
import com.gwynniebee.dao.LeavesDao;
import com.gwynniebee.rest_api.test.helper.LiquibaseOperations;
import com.gwynniebee.rest_api.test.helper.TestDAOBase;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class TestLeavesDao extends TestDAOBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestLeavesDao.class);

    @Test
    public void testQueryLeaves() throws ParseException {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        AttendanceUploadDao a1 = h1.attach(AttendanceUploadDao.class);
        LeavesDao l1 = h1.attach(LeavesDao.class);

        try {
            String lastUpdateBy = "1234";
            String employeeId = "in1";
            String email = "sgupta@gwynniebee.com";
            String firstName = "sumit";
            String lastName = "gupta";
            String dob = "1991-04-06";
            String doj = "2014-07-01";
            String bloodGroup = "o+";
            String employmentStatus = "employed"; // ENUM('employeed','retired'),
            String designation = "se";

            PersonalDetails details = new PersonalDetails();
            details.setlastUpdateBy(lastUpdateBy);
            details.setEmployeeId(employeeId);
            details.setEmail(email);
            details.setFirstName(firstName);
            details.setLastName(lastName);
            details.setDob(dob);
            details.setDoj(doj);
            details.setBloodGroup(bloodGroup);
            details.setEmploymentStatus(employmentStatus);
            details.setDesignation(designation);

            String strDate1 = "2014-06-01";
            String strDate2 = "2014-06-02";
            String strDate3 = "2014-06-03";
            String strDate4 = "2014-06-04";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date1 = sdf.parse(strDate1);
            java.util.Date date2 = sdf.parse(strDate2);
            java.util.Date date3 = sdf.parse(strDate3);
            java.util.Date date4 = sdf.parse(strDate4);

            java.sql.Date sqlDate1 = new Date(date1.getTime());
            java.sql.Date sqlDate2 = new Date(date2.getTime());
            java.sql.Date sqlDate3 = new Date(date3.getTime());
            java.sql.Date sqlDate4 = new Date(date4.getTime());

            h1.begin();
            e1.addPersoDetails(details, "1234", "1234");
            a1.updateattendance("1234", "absent", sqlDate1, "1234");
            a1.updateattendance("1234", "present", sqlDate2, "1234");
            a1.updateattendance("1234", "absent", sqlDate3, "1234");
            h1.commit();
            List<LeavesDetails> resp_leaves = l1.queryleaves("1234", sqlDate1, sqlDate4);
            for (LeavesDetails leave : resp_leaves) {
                System.out.println(leave.getStatus());
            }

        } catch (Exception e) {
            assertEquals(MySQLIntegrityConstraintViolationException.class, e.getCause().getClass());
        } finally {
            h1.close();
        }

    }

    @Test
    public void testQueryLeaves_nodata() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        LeavesDao l1 = h1.attach(LeavesDao.class);
        try {
            String strDate1 = "2014-06-01";
            String strDate4 = "2014-06-04";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date1 = sdf.parse(strDate1);
            java.util.Date date4 = sdf.parse(strDate4);

            java.sql.Date sqlDate1 = new Date(date1.getTime());
            java.sql.Date sqlDate4 = new Date(date4.getTime());
            List<LeavesDetails> resp_leaves = l1.queryleaves("1234", sqlDate1, sqlDate4);
        } catch (Exception e) {
            assertEquals(NullPointerException.class, e.getCause().getClass());
        } finally {
            h1.close();
        }
    }

    @Test
    public void testQueryLeaves_fkConstraint() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        AttendanceUploadDao a1 = h1.attach(AttendanceUploadDao.class);
        try {
            String strDate1 = "2014-06-01";
            String strDate2 = "2014-06-02";
            String strDate3 = "2014-06-03";
            String strDate4 = "2014-06-04";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date1 = sdf.parse(strDate1);
            java.util.Date date2 = sdf.parse(strDate2);
            java.util.Date date3 = sdf.parse(strDate3);
            java.util.Date date4 = sdf.parse(strDate4);

            java.sql.Date sqlDate1 = new Date(date1.getTime());
            java.sql.Date sqlDate2 = new Date(date2.getTime());
            java.sql.Date sqlDate3 = new Date(date3.getTime());
            java.sql.Date sqlDate4 = new Date(date4.getTime());
            h1.begin();
            a1.updateattendance("1234", "absent", sqlDate1, "1234");
            a1.updateattendance("1234", "present", sqlDate2, "1234");
            a1.updateattendance("1234", "absent", sqlDate3, "1234");
            h1.commit();

        } catch (Exception e) {
            assertEquals(MySQLIntegrityConstraintViolationException.class, e.getCause().getClass());
        } finally {
            h1.close();
        }
    }

    /*
     * A Test to Check Not Null fields of database
     */
    @Test
    public void testQueryLeaves_Null() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        AttendanceUploadDao a1 = h1.attach(AttendanceUploadDao.class);
        try {

            String strDate1 = "2014-06-01";
            String strDate2 = "2014-06-02";
            String strDate3 = "2014-06-03";
            String strDate4 = "2014-06-04";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date date1 = sdf.parse(strDate1);
            java.util.Date date2 = sdf.parse(strDate2);
            java.util.Date date3 = sdf.parse(strDate3);
            java.util.Date date4 = sdf.parse(strDate4);

            java.sql.Date sqlDate1 = new Date(date1.getTime());
            java.sql.Date sqlDate2 = new Date(date2.getTime());
            java.sql.Date sqlDate3 = new Date(date3.getTime());

            h1.begin();
            a1.updateattendance("1234", null, sqlDate1, "1234");
            a1.updateattendance("1234", "present", sqlDate2, "1234");
            a1.updateattendance("1234", "absent", sqlDate3, "1234");
            h1.commit();
        } catch (Exception e) {
            assertEquals(MySQLIntegrityConstraintViolationException.class, e.getCause().getClass());
        } finally {
            h1.close();
        }
    }
}
