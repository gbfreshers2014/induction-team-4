package com.gwynniebee.rest_api.test.helper;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;

import javax.sql.DataSource;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.DateTimeAF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.RestApplication.restlet.RestApplication;
import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.Constants;
import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.LoginCred;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.backoffice.restlet.responses.Details;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.entites.AddEmpEntity;
import com.gwynniebee.entites.AttendanceEntityManager;
import com.gwynniebee.entites.BaseEntityManager;
import com.gwynniebee.entites.LeavesEntityManager;
import com.gwynniebee.entites.TaskEntityManager;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.test.helper.j2ee.J2eeJndiHelper;

public class TestEntityManagerBase {

    private static final Logger LOG = LoggerFactory.getLogger(TestEntityManagerBase.class);

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        LOG.info("Executing BeforeClass: Creating databases and apply schema through Liquibase");
        LiquibaseOperations.completeLiquibaseReset();

        J2eeJndiHelper.reset();
        DataSource ds = J2eeJndiHelper.getDataSource("configuration/local/emp_prop.properties", "gwynniebee_users");
        J2eeJndiHelper.bind(Constants.DATABASE_SOURCE_NAME, ds);

        // GBUsersApplication.setDbi(LiquibaseOperations.getDBI());
        DBI dbi = new DBI(ds);
        dbi.registerArgumentFactory(new DateTimeAF());
        RestApplication.setDbi(dbi);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        LiquibaseOperations.dropDatabase();
        LOG.info("Executing AfterClass: Dropping databases");
        J2eeJndiHelper.destroy();
    }

    @Before
    public void setUp() throws Exception {
        LOG.debug("Setup test: Cleaning all tables");
        LiquibaseOperations.cleanTables();
    }

}
