/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.rest_api.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.LoginResponseObject;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.dao.EmpPerDetaildao;
import com.gwynniebee.dao.LoginDao;
import com.gwynniebee.dao.LoginInternalDao;
import com.gwynniebee.rest_api.test.helper.LiquibaseOperations;
import com.gwynniebee.rest_api.test.helper.TestDAOBase;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

/**
 * Base class for DAO Test.
 * @author sumit
 */

public class TestLoginDao extends TestDAOBase {

    public static final Logger LOG = LoggerFactory.getLogger(TestLoginDao.class);

    @Test
    public void testlogin() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = dbi.open();
        LoginDao l1 = h1.attach(LoginDao.class);
        LoginInternalDao l2 = h1.attach(LoginInternalDao.class);
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        try {
            String lastUpdateBy = "1234";
            String employeeId = "in1";
            String email = "sgupta@gwynniebee.com";
            String firstName = "sumit";
            String lastName = "gupta";
            String dob = "1991-04-06";
            String doj = "2014-07-01";
            String bloodGroup = "o+";
            String employmentStatus = "employed"; // ENUM('employeed','retired'),
            String designation = "se";
            PersonalDetails details = new PersonalDetails();
            details.setlastUpdateBy(lastUpdateBy);
            details.setEmployeeId(employeeId);
            details.setEmail(email);
            details.setFirstName(firstName);
            details.setLastName(lastName);
            details.setDob(dob);
            details.setDoj(doj);
            details.setBloodGroup(bloodGroup);
            details.setEmploymentStatus(employmentStatus);
            details.setDesignation(designation);
            h1.begin();
            e1.addPersoDetails(details, "1234", "1234");
            e1.addLoginDetails("1234", "sumit", "employee", "1234");
            h1.commit();
            String resp = l2.fetchuuid(email);
            LoginResponseObject resp_login = l1.authenticateuserandspecifyrole(resp, "sumit");
            assertEquals("employee", resp_login.getRole());
            System.out.println(resp);
            assertEquals("1234", resp);
        } catch (Exception e) {
            assertEquals(MySQLIntegrityConstraintViolationException.class, e.getCause().getClass());
        } finally {
            h1.close();
        }
    }

    @Test
    public void check_UniqueEmailConstraint() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = dbi.open();
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        try {
            String lastUpdateBy1 = "1234";
            String employeeId1 = "in1";
            String email1 = "sgupta@gwynniebee.com";
            String firstName1 = "sumit";
            String lastName1 = "gupta";
            String dob1 = "1991-04-06";
            String doj1 = "2014-07-01";
            String bloodGroup1 = "o+";
            String employmentStatus1 = "employed"; // ENUM('employeed','retired'),
            String designation1 = "se";
            PersonalDetails details = new PersonalDetails();
            details.setlastUpdateBy(lastUpdateBy1);
            details.setEmployeeId(employeeId1);
            details.setEmail(email1);
            details.setFirstName(firstName1);
            details.setLastName(lastName1);
            details.setDob(dob1);
            details.setDoj(doj1);
            details.setBloodGroup(bloodGroup1);
            details.setEmploymentStatus(employmentStatus1);
            details.setDesignation(designation1);
            String lastUpdateBy2 = "1234";
            String employeeId2 = "in2";
            String email2 = "sgupta@gwynniebee.com";
            String firstName2 = "sumit";
            String lastName2 = "gupta";
            String dob2 = "1991-04-06";
            String doj2 = "2014-07-01";
            String bloodGroup2 = "o+";
            String employmentStatus2 = "employed"; // ENUM('employeed','retired'),
            String designation2 = "se";
            PersonalDetails details1 = new PersonalDetails();
            details.setlastUpdateBy(lastUpdateBy2);
            details.setEmployeeId(employeeId2);
            details.setEmail(email2);
            details.setFirstName(firstName2);
            details.setLastName(lastName2);
            details.setDob(dob2);
            details.setDoj(doj2);
            details.setBloodGroup(bloodGroup2);
            details.setEmploymentStatus(employmentStatus2);
            details.setDesignation(designation2);
            h1.begin();
            e1.addPersoDetails(details, "1234", "1234");
            e1.addPersoDetails(details, "1235", "1234");
            h1.commit();
        } catch (Exception e) {
            assertEquals(MySQLIntegrityConstraintViolationException.class, e.getCause().getClass());
        } finally {
            h1.close();
        }
    }

    @Test
    public void testlogin_with_null() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = dbi.open();
        LoginDao l1 = h1.attach(LoginDao.class);
        LoginInternalDao l2 = h1.attach(LoginInternalDao.class);
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        LoginResponseObject resp_login = null;
        try {
            String lastUpdateBy = "1234";
            String employeeId = "in1";
            String email = "sgupta@gwynniebee.com";
            String firstName = "sumit";
            String lastName = "gupta";
            String dob = "1991-04-06";
            String doj = "2014-07-01";
            String bloodGroup = "o+";
            String employmentStatus = "employed"; // ENUM('employeed','retired'),
            String designation = "se";

            PersonalDetails details = new PersonalDetails();
            details.setlastUpdateBy(lastUpdateBy);
            details.setEmployeeId(employeeId);
            details.setEmail(email);
            details.setFirstName(firstName);
            details.setLastName(lastName);
            details.setDob(dob);
            details.setDoj(doj);
            details.setBloodGroup(bloodGroup);
            details.setEmploymentStatus(employmentStatus);
            details.setDesignation(designation);
            h1.begin();
            e1.addPersoDetails(details, "1234", "1234");
            e1.addLoginDetails("1234", "sumit", "employee", "1234");
            h1.commit();
            String resp = l2.fetchuuid(email);
            assertEquals("1234", resp);
            resp_login = l1.authenticateuserandspecifyrole("1235", "abc");

        } catch (Exception e) {
            assertEquals(NullPointerException.class, resp_login.getRole());
        } finally {
            h1.close();
        }

    }

    @Test
    public void testLoginDaoInsert() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = dbi.open();
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        try {
            h1.begin();
            e1.addLoginDetails("1234", "sumit", "employee", "1234");
            h1.commit();

        } catch (Exception e) {

            assertEquals(MySQLIntegrityConstraintViolationException.class, e.getCause().getClass());
        } finally {
            h1.close();
        }
    }
}
