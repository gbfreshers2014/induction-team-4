/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.rest_api.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.FamilyDetailsObject;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.dao.EmpPerDetaildao;
import com.gwynniebee.dao.EmployeFamilyDao;
import com.gwynniebee.rest_api.test.helper.LiquibaseOperations;
import com.gwynniebee.rest_api.test.helper.TestDAOBase;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

/**
 * Base class for DAO Test.
 * @author sumit
 */

public class TestEmployeeFamilyDao extends TestDAOBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestEmployeeAddressDao.class);

    @Test
    public void testsqlQueryFamily() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        EmployeFamilyDao e2 = h1.attach(EmployeFamilyDao.class);
        try {
            String lastUpdateBy = "1234";
            String employeeId = "in1";
            String email = "sgupta@gwynniebee.com";
            String firstName = "sumit";
            String lastName = "gupta";
            String dob = "1991-04-06";
            String doj = "2014-07-01";
            String bloodGroup = "o+";
            String employmentStatus = "regular"; // ENUM('employeed','retired'),
            String designation = "se";

            PersonalDetails details = new PersonalDetails();
            details.setlastUpdateBy(lastUpdateBy);
            details.setEmployeeId(employeeId);
            details.setEmail(email);
            details.setFirstName(firstName);
            details.setLastName(lastName);
            details.setDob(dob);
            details.setDoj(doj);
            details.setBloodGroup(bloodGroup);
            details.setEmploymentStatus(employmentStatus);
            details.setDesignation(designation);

            FamilyDetails f1 = new FamilyDetails();
            FamilyDetails f2 = new FamilyDetails();
            String fullName1 = "abc";
            String relation1 = "mother";
            String dependent1 = "1";
            String dob1 = "1961-01-01";
            String bloodGroup1 = "o+";
            String lastUpdateBy1 = "1234";
            f1.setFullName(fullName1);
            f1.setRelation(relation1);
            f1.setDependent(dependent1);
            f1.setDob(dob1);
            f1.setBloodGroup(bloodGroup1);
            f1.setlastUpdateBy(lastUpdateBy1);
            String fullName2 = "xyz";
            String relation2 = "father";
            String dependent2 = "0";
            String dob2 = "1961-02-01";
            String bloodGroup2 = "o+";
            String lastUpdateBy2 = "1234";
            f2.setFullName(fullName2);
            f2.setRelation(relation2);
            f2.setDependent(dependent2);
            f2.setDob(dob2);
            f2.setBloodGroup(bloodGroup2);
            f2.setlastUpdateBy(lastUpdateBy2);
            h1.begin();
            e1.addPersoDetails(details, "1234", "1234");
            e1.addFamDetails("1234", f1, "1234");
            e1.addFamDetails("1234", f2, "1234");
            h1.commit();
            List<FamilyDetailsObject> resp_fam = e2.sqlqueryfamily("1234");
            for (FamilyDetailsObject fam : resp_fam) {
                System.out.println(fam.getFullName());
                System.out.println(fam.isDependent());
            }

        } finally {
            h1.close();
        }
    }

    @Test
    public void testsqlQueryFamily_fkconst() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        EmployeFamilyDao e2 = h1.attach(EmployeFamilyDao.class);
        try {
            FamilyDetails f1 = new FamilyDetails();
            String fullName1 = "abc";
            String relation1 = "mother";
            String dependent1 = "1";
            String dob1 = "1961-01-01";
            String bloodGroup1 = "o+";
            String lastUpdateBy1 = "1234";
            f1.setFullName(fullName1);
            f1.setRelation(relation1);
            f1.setDependent(dependent1);
            f1.setDob(dob1);
            f1.setBloodGroup(bloodGroup1);
            f1.setlastUpdateBy(lastUpdateBy1);
            h1.begin();
            e1.addFamDetails("1234", f1, "1234");
            h1.commit();

        } catch (Exception e) {
            assertEquals(MySQLIntegrityConstraintViolationException.class, e.getCause().getClass());
        } finally {
            h1.close();
        }

    }

    @Test
    public void testsqlQueryFamily_noData() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        EmployeFamilyDao e2 = h1.attach(EmployeFamilyDao.class);
        // try {
        List<FamilyDetailsObject> resp_fam = e2.sqlqueryfamily("1234");
        // } catch (Exception e) {
        assertEquals(true, resp_fam.isEmpty());
        // } finally {
        h1.close();
        // }

    }
}
