/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.rest_api.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetailsObject;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.dao.EmpPerDetaildao;
import com.gwynniebee.dao.EmployeeCommunicationDao;
import com.gwynniebee.rest_api.test.helper.LiquibaseOperations;
import com.gwynniebee.rest_api.test.helper.TestDAOBase;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

/**
 * Base class for DAO Test.
 * @author sumit
 */

public class TestEmployeeCommunicationDao extends TestDAOBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestEmployeeCommunicationDao.class);

    @Test
    public void testEmployeeCommunicationDao() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        try {
            String lastUpdateBy = "1234";
            String employeeId = "in1";
            String email = "sgupta@gwynniebee.com";
            String firstName = "sumit";
            String lastName = "gupta";
            String dob = "1991-04-06";
            String doj = "2014-07-01";
            String bloodGroup = "o+";
            String employmentStatus = "employed"; // ENUM('employeed','retired'),
            String designation = "se";

            PersonalDetails details = new PersonalDetails();
            details.setlastUpdateBy(lastUpdateBy);
            details.setEmployeeId(employeeId);
            details.setEmail(email);
            details.setFirstName(firstName);
            details.setLastName(lastName);
            details.setDob(dob);
            details.setDoj(doj);
            details.setBloodGroup(bloodGroup);
            details.setEmploymentStatus(employmentStatus);
            details.setDesignation(designation);

            CommunicationDetails d1 = new CommunicationDetails();
            String lastUpdatedBy1 = "1234";
            String createdBy1 = "1234";

            String type1 = "phone_no";
            String comm_details1 = "9034832205";

            String lastUpdatedBy2 = "1234";
            String createdBy2 = "1234";

            String type2 = "skype_id";
            String comm_details2 = "s.gupta91";

            h1 = dbi.open();
            EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
            EmployeeCommunicationDao e_com = h1.attach(EmployeeCommunicationDao.class);
            h1.begin();
            e1.addPersoDetails(details, "1234", "1234");
            e1.addCommuDetails("1234", comm_details1, type1, "1234");
            e1.addCommuDetails("1234", comm_details2, type2, "1234");
            h1.commit();
            List<CommunicationDetailsObject> resp_com = e_com.sqlquerycommunication("1234");
            for (CommunicationDetailsObject com : resp_com) {
                System.out.println(com.getDetails());
                System.out.println(com.getType());

            }
        } finally {

            h1.close();

        }
    }

    @Test
    public void testEmployeeCommunicationDao_fkContsraint() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        EmployeeCommunicationDao e_com = h1.attach(EmployeeCommunicationDao.class);
        try {
            CommunicationDetails d1 = new CommunicationDetails();
            String lastUpdatedBy1 = "1234";
            String createdBy1 = "1234";
            String type1 = "phone_no";
            String comm_details1 = "9034832205";
            h1.begin();
            e1.addCommuDetails("1234", comm_details1, type1, "1234");
            h1.commit();
        } catch (Exception e) {
            assertEquals(MySQLIntegrityConstraintViolationException.class, e.getCause().getClass());
        } finally {
            h1.close();
        }
    }

    @Test
    public void testEmployeeCommunication_query() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        EmployeeCommunicationDao e_com = h1.attach(EmployeeCommunicationDao.class);
        try {
            List<CommunicationDetailsObject> resp_com = e_com.sqlquerycommunication("1234");
        } catch (Exception e) {
            assertEquals(NullPointerException.class, e.getCause().getClass());
        } finally {
            h1.close();

        }
    }

    @Test
    public void testEmployeeCommunication_storage() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        try {
            String lastUpdateBy = "1234";
            String employeeId = "in1";
            String email = "sgupta@gwynniebee.com";
            String firstName = "sumit";
            String lastName = "gupta";
            String dob = "1991-04-06";
            String doj = "2014-07-01";
            String bloodGroup = "o+";
            String employmentStatus = "employed"; // ENUM('employeed','retired'),
            String designation = "se";

            PersonalDetails details = new PersonalDetails();
            details.setlastUpdateBy(lastUpdateBy);
            details.setEmployeeId(employeeId);
            details.setEmail(email);
            details.setFirstName(firstName);
            details.setLastName(lastName);
            details.setDob(dob);
            details.setDoj(doj);
            details.setBloodGroup(bloodGroup);
            details.setEmploymentStatus(employmentStatus);
            details.setDesignation(designation);

            CommunicationDetails d1 = new CommunicationDetails();
            String lastUpdatedBy1 = "1234";
            String createdBy1 = "1234";
            String type1 = "phone_no";
            String comm_details1 = "9034832205";
            h1.begin();
            e1.addPersoDetails(details, "1234", "1234");
            e1.addCommuDetails("1234", comm_details1, null, "1234");
            h1.commit();

        } catch (Exception e) {
            assertEquals(MySQLIntegrityConstraintViolationException.class, e.getCause().getClass());
        } finally {
            h1.close();

        }

    }
}
