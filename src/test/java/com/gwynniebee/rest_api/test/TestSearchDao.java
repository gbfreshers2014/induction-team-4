package com.gwynniebee.rest_api.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.backoffice.objects.PersonalDetailsObject;
import com.gwynniebee.backoffice.objects.SearchResult;
import com.gwynniebee.dao.EmpPerDetaildao;
import com.gwynniebee.dao.SearchDao;
import com.gwynniebee.rest_api.test.helper.LiquibaseOperations;
import com.gwynniebee.rest_api.test.helper.TestDAOBase;

public class TestSearchDao extends TestDAOBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestSearchDao.class);

    @Test
    public void testsqlQuerySearchDao() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        EmpPerDetaildao e1 = h1.attach(EmpPerDetaildao.class);
        SearchDao s1 = h1.attach(SearchDao.class);
        try {
            String lastUpdateBy = "1234";
            String employeeId = "in1";
            String email = "sgupta@gwynniebee.com";
            String firstName = "sumit";
            String lastName = "gupta";
            String dob = "1991-04-06";
            String doj = "2014-07-01";
            String bloodGroup = "o+";
            String employmentStatus = "employed"; // ENUM('employeed','retired'),
            String designation = "se";

            PersonalDetails details = new PersonalDetails();
            details.setlastUpdateBy(lastUpdateBy);
            details.setEmployeeId(employeeId);
            details.setEmail(email);
            details.setFirstName(firstName);
            details.setLastName(lastName);
            details.setDob(dob);
            details.setDoj(doj);
            details.setBloodGroup(bloodGroup);
            details.setEmploymentStatus(employmentStatus);
            details.setDesignation(designation);
            h1.begin();
            e1.addPersoDetails(details, "1234", "1234");
            h1.commit();
            List<SearchResult> resp_search = s1.sqlquery("sumit");
            for (SearchResult resp : resp_search) {
                PersonalDetailsObject emp1 = resp.getPersonalDetails();
                System.out.println(emp1.getDesignation());
                System.out.println(emp1.getEmail());
            }
        } finally {
            h1.close();

        }
    }

    /*
     * Test if there is no value in the database
     */
    @Test
    public void testSqlSearchQueryNull() {
        DBI dbi = LiquibaseOperations.getDBI();
        Handle h1 = null;
        h1 = dbi.open();
        SearchDao s1 = h1.attach(SearchDao.class);
        try {
            List<SearchResult> resp_search = s1.sqlquery("sumit");
        } catch (Exception e) {
            assertEquals(NullPointerException.class, e.getCause().getClass());
        } finally {
            h1.close();
        }
    }

}
