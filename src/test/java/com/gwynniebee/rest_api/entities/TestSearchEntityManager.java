package com.gwynniebee.rest_api.entities;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.Test;

import com.gwynniebee.RestApplication.restlet.RestApplication;
import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.LoginCred;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.backoffice.objects.SearchResult;
import com.gwynniebee.backoffice.objects.SearchResultArray;
import com.gwynniebee.backoffice.restlet.responses.Details;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.backoffice.restlet.responses.SearchHandler;
import com.gwynniebee.entites.AddEmpEntity;
import com.gwynniebee.entites.BaseEntityManager;
import com.gwynniebee.entites.SearchEntityManager;
import com.gwynniebee.rest_api.test.helper.TestEntityManagerBase;

public class TestSearchEntityManager extends TestEntityManagerBase {
    @Test
    public void testsearchentity() throws ClassNotFoundException, ParseException {
        BaseEntityManager.setDbi(RestApplication.getDbi());
        HashSet<String> uuidset = new HashSet<String>();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date tempDate = sdf1.parse("2014-05-01");
        java.sql.Date date = new java.sql.Date(tempDate.getTime());
        String uploadinguuid = "1235";
        // uuidset.add();

        String lastUpdateBy = "1234";
        String employeeId = "in1";
        String email = "sgupta@gwynniebee.com";
        String firstName = "sumit";
        String lastName = "gupta";
        String dob = "1991-04-06";
        String doj = "2014-07-01";
        String bloodGroup = "o+";
        String employmentStatus = "regular"; // ENUM('employeed','retired'),
        String designation = "se";

        PersonalDetails details = new PersonalDetails();
        details.setlastUpdateBy(lastUpdateBy);
        details.setEmployeeId(employeeId);
        details.setEmail(email);
        details.setFirstName(firstName);
        details.setLastName(lastName);
        details.setDob(dob);
        details.setDoj(doj);
        details.setBloodGroup(bloodGroup);
        details.setEmploymentStatus(employmentStatus);
        details.setDesignation(designation);

        FamilyDetails f1 = new FamilyDetails();
        String fullName1 = "abc";
        String relation1 = "mother";
        String dependent1 = "true";
        String dob1 = "1961-01-01";
        String bloodGroup1 = "o+";
        String lastUpdateBy1 = "1234";
        f1.setBloodGroup(bloodGroup1);
        f1.setDependent(dependent1);
        f1.setDob(dob1);
        f1.setFullName(fullName1);
        f1.setlastUpdateBy(lastUpdateBy1);
        f1.setRelation(relation1);

        CommunicationDetails c1 = new CommunicationDetails();
        String lastUpdatedBy1 = "1234";
        String createdBy1 = "1234";
        String type1 = "phone-primary";
        String comm_details1 = "9034832205";

        c1.setCreatedBy(createdBy1);
        c1.setDetails(comm_details1);
        c1.setLastUpdateBy(lastUpdateBy1);
        c1.setType(type1);

        AddressDetails addr_details1 = new AddressDetails();
        String type_addr = "permanent";
        String streetAddress1 = "H.no 1095";
        String city1 = "Kalka";
        String state1 = "Haryana";
        String country1 = "IN";
        String zipcode1 = "133302";
        String createdBy_addr = "1234";
        String lastUpdateBy_addr = "1234";

        addr_details1.setCity(city1);
        addr_details1.setCountry(country1);
        addr_details1.setCreatedBy(createdBy_addr);
        addr_details1.setLastUpdateBy(lastUpdateBy_addr);
        addr_details1.setState(state1);
        addr_details1.setStreetAddress(streetAddress1);
        addr_details1.setType(type_addr);
        addr_details1.setZipcode(zipcode1);

        ArrayList<AddressDetails> ad = new ArrayList<AddressDetails>();
        ad.add(addr_details1);

        ArrayList<CommunicationDetails> cd = new ArrayList<CommunicationDetails>();
        cd.add(c1);

        ArrayList<FamilyDetails> fd = new ArrayList<FamilyDetails>();
        fd.add(f1);

        EmployeeDetails e1 = new EmployeeDetails();
        e1.setAddressDetails(ad);
        e1.setCommunicationDetails(cd);
        e1.setFamilyDetails(fd);
        e1.setPersonalDetails(details);

        String updatingUuid = "1234";
        LoginCred loginCred = new LoginCred();
        loginCred.setPassword("1234");
        loginCred.setRole("admin");
        // String uuid = "1234";

        Details obj = new Details();
        obj.setEmployeeDetails(e1);
        obj.setLoginCred(loginCred);
        obj.setUpdatingUuid(updatingUuid);

        AddEmpEntity aee = new AddEmpEntity();
        Generichandler ab = aee.addEmployee(obj);

        ArrayList<String> parameters = new ArrayList<String>();
        parameters.add("sgupta");
        HashMap<String, Integer> hm = new HashMap<String, Integer>();
        SearchHandler response = new SearchHandler();
        SearchHandler resp1 = new SearchHandler();
        int index = 0;
        SearchEntityManager sem = new SearchEntityManager();
        for (String currentQueryParam : parameters) {
            response = sem.searchQuery(currentQueryParam);
            SearchResultArray sra = new SearchResultArray();
            ArrayList<SearchResult> temp = response.getList();
            sra.setEmployeeList(temp);
            System.out.println(temp.size());
            // response.getList().clear();
            if (response.getStatus().getCode() == 0) {
                for (SearchResult sr : sra.getEmployeeList()) {
                    if (!hm.containsKey(sr.getPersonalDetails().getUuid())) {
                        hm.put(sr.getPersonalDetails().getUuid(), index++);
                        resp1.getList().add(sr);
                    } else {
                        int val = response.getList().get(hm.get(sr.getPersonalDetails().getUuid())).getValue();
                        resp1.getList().get(hm.get(sr.getPersonalDetails().getUuid())).setValue(val++);
                    }
                }
            }
        }
        System.out.println(response.getList().size());
        assertEquals(resp1.getList().get(0).getPersonalDetails().getEmail(), obj.getEmployeeDetails().getPersonalDetails().getEmail());
    }

}
