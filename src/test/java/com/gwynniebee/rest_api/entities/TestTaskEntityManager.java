package com.gwynniebee.rest_api.entities;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Test;

import com.gwynniebee.RestApplication.restlet.RestApplication;
import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.LoginCred;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.backoffice.objects.TaskObject;
import com.gwynniebee.backoffice.restlet.responses.Details;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.entites.AddEmpEntity;
import com.gwynniebee.entites.BaseEntityManager;
import com.gwynniebee.entites.TaskEntityManager;
import com.gwynniebee.rest_api.test.helper.TestEntityManagerBase;

public class TestTaskEntityManager extends TestEntityManagerBase {

    @Test
    public void testask() throws ParseException {
        BaseEntityManager.setDbi(RestApplication.getDbi());
        HashSet<String> uuidset = new HashSet<String>();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date tempDate = sdf1.parse("2014-05-01");
        java.sql.Date date = new java.sql.Date(tempDate.getTime());
        String uploadinguuid = "1235";
        // uuidset.add();

        TaskEntityManager tem = new TaskEntityManager();
        tem.updatetasks(date, "processed", "1235");

        TaskObject res = tem.gettasks();
        assertEquals(res.getTask().get(0).getAction(), "processed");

    }
}
