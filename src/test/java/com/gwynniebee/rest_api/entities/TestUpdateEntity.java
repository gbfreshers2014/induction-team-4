package com.gwynniebee.rest_api.entities;

import java.util.ArrayList;

import org.junit.Test;

import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.LoginCred;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.backoffice.restlet.responses.Details;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.entites.UpdateDetailsEntity;

public class TestUpdateEntity {
    @Test
    public void testupdate() throws ClassNotFoundException {
        Generichandler gh = new Generichandler();
        String updatinguuid = "1235";
        String lastUpdateBy = "1234";
        String employeeId = "in1";
        String email = "sgupta@gwynniebee.com";
        String firstName = "sumit";
        String lastName = "gupta";
        String dob = "1991-04-06";
        String doj = "2014-07-01";
        String bloodGroup = "o+";
        String employmentStatus = "regular"; // ENUM('regular','retired','intern','third-party'),
        String designation = "se";

        PersonalDetails details = new PersonalDetails();
        details.setlastUpdateBy(lastUpdateBy);
        details.setEmployeeId(employeeId);
        details.setEmail(email);
        details.setFirstName(firstName);
        details.setLastName(lastName);
        details.setDob(dob);
        details.setDoj(doj);
        details.setBloodGroup(bloodGroup);
        details.setEmploymentStatus(employmentStatus);
        details.setDesignation(designation);

        FamilyDetails f1 = new FamilyDetails();
        String fullName1 = "abc";
        String relation1 = "mother";
        String dependent1 = "1";
        String dob1 = "1961-01-01";
        String bloodGroup1 = "o+";
        String lastUpdateBy1 = "1234";
        f1.setBloodGroup(bloodGroup1);
        f1.setDependent(dependent1);
        f1.setDob(dob1);
        f1.setFullName(fullName1);
        f1.setlastUpdateBy(lastUpdateBy1);
        f1.setRelation(relation1);

        CommunicationDetails c1 = new CommunicationDetails();
        String lastUpdatedBy1 = "1234";
        String createdBy1 = "1234";
        String type1 = "phone-primary";
        String comm_details1 = "9034832205";

        c1.setCreatedBy(createdBy1);
        c1.setDetails(comm_details1);
        c1.setLastUpdateBy(lastUpdateBy1);
        c1.setType(type1);

        AddressDetails addr_details1 = new AddressDetails();
        String type_addr = "present";
        String streetAddress1 = "H.no 1095";
        String city1 = "Kalka";
        String state1 = "Haryana";
        String country1 = "IN";
        String zipcode1 = "133302";
        String createdBy_addr = "1234";
        String lastUpdateBy_addr = "1234";

        addr_details1.setCity(city1);
        addr_details1.setCountry(country1);
        addr_details1.setCreatedBy(createdBy_addr);
        addr_details1.setLastUpdateBy(lastUpdateBy_addr);
        addr_details1.setState(state1);
        addr_details1.setStreetAddress(streetAddress1);
        addr_details1.setType(type_addr);
        addr_details1.setZipcode(zipcode1);

        EmployeeDetails ed = new EmployeeDetails();

        ArrayList<FamilyDetails> fd = new ArrayList<FamilyDetails>();
        fd.add(f1);
        ed.setFamilyDetails(fd);

        ArrayList<AddressDetails> addr = new ArrayList<AddressDetails>();
        addr.add(addr_details1);
        ed.setAddressDetails(addr);

        ArrayList<CommunicationDetails> comm = new ArrayList<CommunicationDetails>();
        comm.add(c1);
        ed.setCommunicationDetails(comm);

        ed.setPersonalDetails(details);

        LoginCred lc = new LoginCred();
        lc.setRole("admin");
        lc.setPassword("12345");
        lc.setLastUpdateBy(updatinguuid);

        Details obj = new Details();
        obj.setEmployeeDetails(ed);
        obj.setLoginCred(lc);
        obj.setUpdatingUuid(updatinguuid);

        UpdateDetailsEntity ude = new UpdateDetailsEntity();
        gh = ude.updateDetails(obj);
        System.out.println(gh.getStatus());
    }

}
