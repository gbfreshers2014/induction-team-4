package com.gwynniebee.rest_api.entities;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.DateTimeAF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.Constants;
import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.LoginCred;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.backoffice.restlet.responses.Details;
import com.gwynniebee.entites.AddEmpEntity;
import com.gwynniebee.entites.AttendanceEntityManager;
import com.gwynniebee.entites.BaseEntityManager;
import com.gwynniebee.entites.LeavesEntityManager;
import com.gwynniebee.entites.TaskEntityManager;
import com.gwynniebee.rest_api.test.helper.TestEntityManagerBase;
import com.gwynniebee.test.helper.j2ee.J2eeJndiHelper;

public class TestAddEmpEnity extends TestEntityManagerBase {
    public static final Logger LOG = LoggerFactory.getLogger(TestAddEmpEnity.class);

    @Test
    public void testAddEmpEnity() throws IOException, ParseException {

        J2eeJndiHelper.reset();
        DataSource ds = J2eeJndiHelper.getDataSource("configuration/local/emp_prop.properties", "gwynniebee_users.url");
        J2eeJndiHelper.bind(Constants.DATABASE_SOURCE_NAME, ds);

        // GBUsersApplication.setDbi(LiquibaseOperations.getDBI());
        DBI dbi = new DBI(ds);
        dbi.registerArgumentFactory(new DateTimeAF());
        BaseEntityManager.setDbi(dbi);
        HashSet<String> uuidset = new HashSet<String>();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date tempDate = sdf1.parse("2014-01-01");
        java.sql.Date date = new java.sql.Date(tempDate.getTime());
        String uploadinguuid = "1235";
        uuidset.add("root@gwynniebee.com");

        String lastUpdateBy = "1234";
        String employeeId = "in1";
        String email = "sgupta@gwynniebee.com";
        String firstName = "sumit";
        String lastName = "gupta";
        String dob = "1991-04-06";
        String doj = "2014-07-01";
        String bloodGroup = "o+";
        String employmentStatus = "employed"; // ENUM('employeed','retired'),
        String designation = "se";

        PersonalDetails details = new PersonalDetails();
        details.setlastUpdateBy(lastUpdateBy);
        details.setEmployeeId(employeeId);
        details.setEmail(email);
        details.setFirstName(firstName);
        details.setLastName(lastName);
        details.setDob(dob);
        details.setDoj(doj);
        details.setBloodGroup(bloodGroup);
        details.setEmploymentStatus(employmentStatus);
        details.setDesignation(designation);

        FamilyDetails f1 = new FamilyDetails();
        String fullName1 = "abc";
        String relation1 = "mother";
        String dependent1 = "1";
        String dob1 = "1961-01-01";
        String bloodGroup1 = "o+";
        String lastUpdateBy1 = "1234";
        f1.setBloodGroup(bloodGroup1);
        f1.setDependent(dependent1);
        f1.setDob(dob1);
        f1.setFullName(fullName1);
        f1.setlastUpdateBy(lastUpdateBy1);
        f1.setRelation(relation1);

        CommunicationDetails c1 = new CommunicationDetails();
        String lastUpdatedBy1 = "1234";
        String createdBy1 = "1234";
        String type1 = "phone_no";
        String comm_details1 = "9034832205";

        c1.setCreatedBy(createdBy1);
        c1.setDetails(comm_details1);
        c1.setLastUpdateBy(lastUpdateBy1);
        c1.setType(type1);

        AddressDetails addr_details1 = new AddressDetails();
        String type_addr = "permanent";
        String streetAddress1 = "H.no 1095";
        String city1 = "Kalka";
        String state1 = "Haryana";
        String country1 = "IN";
        String zipcode1 = "133302";
        String createdBy_addr = "1234";
        String lastUpdateBy_addr = "1234";

        addr_details1.setCity(city1);
        addr_details1.setCountry(country1);
        addr_details1.setCreatedBy(createdBy_addr);
        addr_details1.setLastUpdateBy(lastUpdateBy_addr);
        addr_details1.setState(state1);
        addr_details1.setStreetAddress(streetAddress1);
        addr_details1.setType(type_addr);
        addr_details1.setZipcode(zipcode1);

        ArrayList<AddressDetails> ad = new ArrayList<AddressDetails>();
        ad.add(addr_details1);

        ArrayList<CommunicationDetails> cd = new ArrayList<CommunicationDetails>();
        cd.add(c1);

        ArrayList<FamilyDetails> fd = new ArrayList<FamilyDetails>();
        fd.add(f1);

        EmployeeDetails e1 = new EmployeeDetails();
        e1.setAddressDetails(ad);
        e1.setCommunicationDetails(cd);
        e1.setFamilyDetails(fd);
        e1.setPersonalDetails(details);

        String updatingUuid = "1234";
        LoginCred loginCred = new LoginCred();
        loginCred.setPassword("1234");
        loginCred.setRole("admin");
        String uuid = "1234";

        Details obj = new Details();
        obj.setEmployeeDetails(e1);
        obj.setLoginCred(loginCred);
        obj.setUpdatingUuid(updatingUuid);
        obj.setUuid(uuid);
        AddEmpEntity aee = new AddEmpEntity();
        aee.addEmployee(obj);

        AttendanceEntityManager aer = new AttendanceEntityManager();
        aer.updateAttendance(uuidset, date, uploadinguuid);
        TaskEntityManager tem = new TaskEntityManager();
    //    tem.updatetasks(udate, "processed", "1235");

        LeavesEntityManager lem = new LeavesEntityManager();
        
        
    }
}
