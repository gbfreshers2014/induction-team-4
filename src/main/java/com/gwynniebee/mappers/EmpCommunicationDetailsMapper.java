/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.CommunicationDetails;

/**
 * @author rahul
 */
// @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
// @JsonIgnoreProperties(ignoreUnknown = true)
public class EmpCommunicationDetailsMapper implements ResultSetMapper<CommunicationDetails> {
    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginMapper.class);
    @Override
    public CommunicationDetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        CommunicationDetails lro = null;
        if (r != null) {

            lro = new CommunicationDetails();
            // LOG.info("came here ");
            // LOG.info(r.getString("role") + r.getString("UUID"));
            // if (r.first() == true) {
            // lro.status = "000";
            // lro.created_on = r.getString("created_on");
            lro.setDetails(r.getString("details"));
            // lro.created_by = r.getString("created_by");
            lro.setType(r.getString("communication_type"));

            lro.setLastUpdateBy(r.getString("last_updated_by"));
            lro.setLastUpdatedOn(r.getString("last_updated_on"));
            lro.setCreatedBy(r.getString("created_by"));
            lro.setCreatedOn(r.getString("created_on"));
            // lro.dob = r.getString("dob");

        }
        return lro;
    }
}
