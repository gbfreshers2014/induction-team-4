/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Result set mapper for personal employee details for login credential.
 * @author skishore
 */
public class LoginInternalMapper implements ResultSetMapper<String> {

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */
    @Override
    public String map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        // TODO Auto-generated method stub
        return r.getString("uuid");
    }

}
