/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.FamilyDetails;

/**
 * @author rahul
 */
// @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
// @JsonIgnoreProperties(ignoreUnknown = true)
public class EmpFamilyDetailsMapper implements ResultSetMapper<FamilyDetails> {
    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginMapper.class);
    @Override
    public FamilyDetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        FamilyDetails lro = null;
        // LOG.info("came here ");
        // LOG.info(r.getString("role") + r.getString("UUID"));
        if (r != null) {
            lro = new FamilyDetails();
            // lro.status = "000";
            // lro.created_on = r.getString("created_on");
            lro.setFullName(r.getString("full_name"));
            // lro.created_by = r.getString("created_by");
            lro.setDependent(r.getString("dependent"));

            lro.setDob(r.getString("dob"));

            lro.setBloodGroup(r.getString("blood_group"));
            lro.setRelation(r.getString("relation"));
            lro.setCreatedOn(r.getString("created_on"));
            lro.setlastUpdateBy(r.getString("last_updated_by"));
            lro.setLastUpdatedOn(r.getString("last_updated_on"));
            lro.setCreatedBy(r.getString("created_by"));
            lro.setDependentID(r.getString("dep_id"));

            // lro.last_updated_by = r.getString("last_updated_by");
            // lro.last_updated_on = r.getString("last_updated_on");

        }

        // lro.firstName = r.getString("firstName");
        // lro.created_by = "raa";
        // lro.employeeId = r.getString("firstName");
        // System.out.println("sdadasdsadqs");
        return lro;
    }
}
