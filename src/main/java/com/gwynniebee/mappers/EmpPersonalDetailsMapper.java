/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.PersonalDetails;

/**
 * @author rahul
 */
// @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
// @JsonIgnoreProperties(ignoreUnknown = true)
public class EmpPersonalDetailsMapper implements ResultSetMapper<PersonalDetails> {
    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginMapper.class);
    @Override
    public PersonalDetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        PersonalDetails lro = null;
        // LOG.info("came here ");
        // LOG.info(r.getString("role") + r.getString("UUID"));
        if (r != null) {
            lro = new PersonalDetails();
            // lro.status = "000";
            lro.setEmployeeId(r.getString("emp_id"));
            // lro.created_by = r.getString("created_by");
            // lro.created_on = r.getString("created_on");
            // lro.uuid = r.getString("uuid");
            lro.setDob(r.getString("dob"));
            lro.setDoj(r.getString("doj"));
            lro.setEmploymentStatus(r.getString("employement_status"));
            lro.setFirstName(r.getString("first_name"));
            lro.setLastName(r.getString("last_name"));
            lro.setEmail(r.getString("email_id"));
            lro.setBloodGroup(r.getString("emp_blood_group"));
            lro.setDesignation(r.getString("designation"));
            lro.setlastUpdateBy(r.getString("last_updated_by"));
            lro.setLastUpdatedOn(r.getString("last_updated_on"));
            lro.setCreatedBy(r.getString("created_by"));
            lro.setCreatedOn(r.getString("created_on"));

            // lro.last_updated_by = r.getString("last_updated_by");
            // lro.last_updated_on = r.getString("last_updated_on");

        }

        // lro.firstName = r.getString("firstName");
        // lro.created_by = "raa";
        // lro.employeeId = r.getString("firstName");
        // System.out.println("sdadasdsadqs");
        return lro;
    }
}
