/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.LeavesDetails;
import com.gwynniebee.backoffice.restlet.responses.LeavesResponse;

/**
 * Result set mapper for attendanc register table.
 * @author skishore
 */
public class LeavesMapper implements ResultSetMapper<LeavesDetails> {

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */
    @Override
    public LeavesDetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        LeavesResponse lr = new LeavesResponse();
        lr.setCount(0);
        LeavesDetails ld = new LeavesDetails();
        ld.setDate(r.getDate("attendance_date"));
        ld.setStatus(r.getString("attendance_status"));
        return ld;
    }

}
