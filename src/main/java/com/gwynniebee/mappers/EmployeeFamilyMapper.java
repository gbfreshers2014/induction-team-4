/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.FamilyDetailsObject;
import com.gwynniebee.backoffice.restlet.resources.EmployeeManage;

/**
 * Result set mapper for Employee Family table for search.
 * @author skishore
 */
public class EmployeeFamilyMapper implements ResultSetMapper<FamilyDetailsObject> {
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeManage.class);

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */

    @Override
    public FamilyDetailsObject map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        LOG.debug("mapping family");
        FamilyDetailsObject fmd = new FamilyDetailsObject();
        fmd.setBloodGroup(r.getString("blood_group"));
        fmd.setDependent(r.getBoolean("dependent"));
        fmd.setDOB(r.getDate("dob"));
        fmd.setFullName(r.getString("full_name"));
        return fmd;
    }

}
