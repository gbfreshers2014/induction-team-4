/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.Taskdetails;

/**
 * Result set mapper for Tasks.
 * @author skishore
 */
public class TaskMapper implements ResultSetMapper<Taskdetails> {

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */
    @Override
    public Taskdetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {

        Taskdetails tdea = new Taskdetails();
        tdea.setAction(r.getString("attendance_action"));
        tdea.setCreatedby(r.getString("action_done_by"));
        tdea.setDate(r.getDate("attendance_date"));
        return tdea;
    }

}
