/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.PersonalDetailsObject;
import com.gwynniebee.backoffice.objects.SearchResult;

/**
 * Result set mapper for Employee Personal Details for search.
 * @author skishore
 */
public class SearchMapper implements ResultSetMapper<SearchResult> {

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */
    @Override
    public SearchResult map(int index, ResultSet r, StatementContext ctx) throws SQLException {

        SearchResult sra = new SearchResult();
        PersonalDetailsObject personalDetails =
                new PersonalDetailsObject(r.getString("uuid"), r.getString("first_name"), r.getString("last_name"),
                        r.getString("email_id"));
        personalDetails.setBloodGroup(r.getString("emp_blood_group"));
        personalDetails.setDesignation(r.getString("designation"));
        personalDetails.setEmploymentStatus(r.getString("employement_status"));
        sra.setPersonalDetails(personalDetails);
        return sra;
    }

}
