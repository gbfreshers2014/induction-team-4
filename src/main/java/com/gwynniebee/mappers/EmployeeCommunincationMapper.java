/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.CommunicationDetailsObject;

/**
 * Result set mapper for Employee Communication table for search.
 * @author skishore
 */
public class EmployeeCommunincationMapper implements ResultSetMapper<CommunicationDetailsObject> {

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */
    @Override
    public CommunicationDetailsObject map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        CommunicationDetailsObject cmd = new CommunicationDetailsObject();
        cmd.setDetails(r.getString("details"));
        cmd.setType(r.getString("communication_type"));
        return cmd;
    }

}
