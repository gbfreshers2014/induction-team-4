/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.AddressDetails;

/**
 * @author rahul
 */
// @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
// @JsonIgnoreProperties(ignoreUnknown = true)
public class EmpAddressDetailsMapper implements ResultSetMapper<AddressDetails> {
    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginMapper.class);
    @Override
    public AddressDetails map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        AddressDetails lro = null;
        // LOG.info("came here ");
        // LOG.info(r.getString("role") + r.getString("UUID"));
        if (r != null) {
            lro = new AddressDetails();
            // lro.status = "000";
            // lro.created_on = r.getString("created_on");
            lro.setCity(r.getString("city"));
            // lro.created_by = r.getString("created_by");
            lro.setCountry(r.getString("country_code"));
            lro.setState(r.getString("state"));

            lro.setStreetAddress(r.getString("street_address"));

            lro.setType(r.getString("address_type"));
            // lro.employmentStatus = r.getString("employement_status");
            // lro.firstName = r.getString("first_name");
            // lro.lastName = r.getString("last_name");
            lro.setZipcode(r.getString("zip_code"));
            // lro.bloodGroup = r.getString("blood_group");
            // lro.relation = r.getString("relation");

            lro.setLastUpdateBy(r.getString("last_updated_by"));
            if (!r.getString("last_updated_on").equals("0000-00-00 00:00:00")) {
                lro.setLastUpdatedOn(r.getString("last_updated_on"));
            }

            lro.setCreatedBy(r.getString("created_by"));
            if (!r.getString("created_on").equals("0000-00-00 00:00:00")) {
                lro.setCreatedOn(r.getString("created_on"));
            }
        }

        // lro.firstName = r.getString("firstName");
        // lro.created_by = "raa";
        // lro.employeeId = r.getString("firstName");
        // System.out.println("sdadasdsadqs");
        return lro;
    }
}
