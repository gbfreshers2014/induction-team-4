/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.LoginCred;

/**
 * @author rahul
 */
// @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
// @JsonIgnoreProperties(ignoreUnknown = true)
public class LoginDetailsMapper implements ResultSetMapper<LoginCred> {
    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginMapper.class);
    @Override
    public LoginCred map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        LoginCred lro = null;
        // LOG.info("came here ");
        // LOG.info(r.getString("role") + r.getString("UUID"));
        if (r != null) {
            lro = new LoginCred();
            // lro.status = "000";
            lro.setPassword(r.getString("password"));
            lro.setRole(r.getString("employee_role"));
            lro.setLastUpdateBy(r.getString("last_updated_by"));

        }

        // lro.firstName = r.getString("firstName");
        // lro.created_by = "raa";
        // lro.employeeId = r.getString("firstName");
        // System.out.println("sdadasdsadqs");
        return lro;
    }
}
