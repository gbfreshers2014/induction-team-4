/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

/**
 * Generates Dates in a list for notifiacation.
 * @author skishore
 */
public class NotificationMapper implements ResultSetMapper<Date> {

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */
    @Override
    public Date map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        return r.getDate("attendance_date");
    }
}
