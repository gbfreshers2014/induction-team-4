/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.LoginResponseObject;

/**
 * Result set mapper for Login Credentials.
 * @author skishore
 */
public class LoginMapper implements ResultSetMapper<LoginResponseObject> {
    private static final Logger LOG = LoggerFactory.getLogger(LoginMapper.class);

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */
    @Override
    public LoginResponseObject map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        LoginResponseObject lro = new LoginResponseObject();
        LOG.info("came here ");
        LOG.info(r.getString("employee_role") + r.getString("uuid"));
        lro.setRole(r.getString("employee_role"));
        lro.setUUID(r.getString("uuid"));
        return lro;
    }

}
