/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.AddressDetailsObject;

/**
 * Result set mapper for Employee table for search.
 * @author skishore
 */
public class EmployeeAddressMapper implements ResultSetMapper<AddressDetailsObject> {

    /*
     * (non-Javadoc)
     * @see org.skife.jdbi.v2.tweak.ResultSetMapper#map(int, java.sql.ResultSet,
     * org.skife.jdbi.v2.StatementContext)
     */
    @Override
    public AddressDetailsObject map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        AddressDetailsObject addr = new AddressDetailsObject();
        addr.setStreetAddress(r.getString("street_address"));
        addr.setCity(r.getString("city"));
        addr.setCountrycode(r.getString("country_code"));
        addr.setState(r.getString("state"));
        addr.setZipcode(r.getString("zip_code"));
        return addr;
    }

}
