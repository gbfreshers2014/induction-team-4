/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.utils;

import java.util.Date;
import java.util.ArrayList;

/**
 * @author skishore genertic methods.
 */
public class Utility {
    /**
     * @param inputs string
     * @return hashcode
     */
    String generatecode(ArrayList<String> inputs) {
        StringBuilder sb = new StringBuilder();
        for (String it : inputs) {
            sb.append(it);
        }
        Date d = new Date();
        sb.append(d.getTime());
        int code = sb.hashCode();
        return String.valueOf(code);
    }

}
