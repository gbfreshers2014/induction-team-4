/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.utils;

/**
 * Resource paths urls.
 * @author Rahul
 */
public final class ResourcePathUrls {

    private ResourcePathUrls() {

    }

    // {UUID}.json
    public static final String URL_MANAGE_EMPLOYEE = "/employee-portal/employees.json";
    public static final String URL_DETAILS_EMPLOYEE = "/employee-portal/employees/%s.json";
    public static final String URL_AUTHENTICATION = "/employee-portal/authenticate.json";
    public static final String URL_EMPLOYEE_LEAVE = "/employee-portal/employees/%s/leaves.json";
    public static final String URL_EMPLOYEE_EXISTS = "/employee-portal/employees/%s/eexists.json";

    public static final String URL_LEAVE = "/employee-portal/leaves.json";
    public static final String URL_ATTENDANCE_TASK = "/employee-portal/attendancetask.json";
    public static final String URL_NOTIFICATION = "/employee-portal/notification.json";
    public static final String URL_ADMIN_EMAILS = "/admin_emails.json";

    /**
     * @return returns the url of notification of pending attendance.
     */
    public static String notification() {
        return URL_NOTIFICATION;
    }

    /**
     * @return returns the url corresponds to the attendance task.
     */
    public static String attendanceTask() {
        return URL_ATTENDANCE_TASK;
    }

    /**
     * @return returns the url corresponds to the leave of the users.
     */
    public static String leavesUrl() {

        return URL_LEAVE;

    }

    /**
     * @return returns url of authentication.
     */
    public static String loginAuthentication() {
        return URL_AUTHENTICATION;
    }

    /**
     * @return returns the url for employee management.
     */
    public static String getManageEmployee() {
        return URL_MANAGE_EMPLOYEE;
    }

    /**
     * @param uuid to be set
     * @return attached string
     */
    public static String employeeExists(String uuid) {
        return attachEmployeeIdtoExists("{" + uuid + "}");
    }

    /**
     * @param uuid uuid of the employee.
     * @return attaches the uuid constant to the url.
     */
    public static String attachEmployeeIdtoExists(String uuid) {
        return String.format(URL_EMPLOYEE_EXISTS, uuid);
    }

    /**
     * @param uuid uuid of the employee.
     * @return attaches the uuid constant to the url.
     */
    public static String attachEmployeeId(String uuid) {
        return String.format(URL_DETAILS_EMPLOYEE, uuid);
    }

    /**
     * @param uuid uuid of the user.
     * @return returns the exact url which is used to get emplyee information.
     */
    public static String getEmployeeInfo(String uuid) {
        return attachEmployeeId("{" + uuid + "}");

    }

    /**
     * @param uuid uuid of the user.
     * @return return the url corresponds to the leave of the user.
     */
    public static String employeeLeavesUrl(String uuid) {

        String temp = "{" + uuid + "}";
        return String.format(URL_EMPLOYEE_LEAVE, temp);

    }

    /**
     * @return returns the url for admin_email.
     */
    public static String adminEmails() {
        return URL_ADMIN_EMAILS;
    }

    /**
     * @param ridAttr name of rid in the path
     * @param namespaceIdAttr name of namespace in the path
     * @return route for namespace rid Item fulfillment resource
     */
    // public static String getRidItemFulfillmentResourceRouteOld(String
    // ridAttr, String namespaceIdAttr) {
    // return getRidItemFulfillmentResourcePath("{" + ridAttr + "}", "{" +
    // namespaceIdAttr + "}");
    // }
}
