/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.RestApplication.restlet;

import java.util.Properties;

import javax.validation.Validator;

import org.restlet.Application;
import org.restlet.data.MediaType;
import org.restlet.routing.Router;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.DBConstants;
import com.gwynniebee.backoffice.restlet.resources.AdminEmailsForTeam1;
import com.gwynniebee.backoffice.restlet.resources.AsyncAttendanceUpdateASR;
import com.gwynniebee.backoffice.restlet.resources.AttendanceUploadASR;
import com.gwynniebee.backoffice.restlet.resources.EmployeeExists;
import com.gwynniebee.backoffice.restlet.resources.EmployeeManage;
import com.gwynniebee.backoffice.restlet.resources.EmployeePersonalDetails;
import com.gwynniebee.backoffice.restlet.resources.GenerateNotificationASR;
import com.gwynniebee.backoffice.restlet.resources.LeavesASR;
import com.gwynniebee.backoffice.restlet.resources.LoginASR;
import com.gwynniebee.entites.BaseEntityManager;
import com.gwynniebee.iohelpers.IOGBUtils;
import com.gwynniebee.rest.service.restlet.GBRestletApplication;
import com.gwynniebee.utils.ResourcePathUrls;

//import com.gwynniebee.replenishment.service.resources.*;

/**
 * routing and controlling Resources.
 * @author Shyam,Sumit,Rahul sharma
 */

public class RestApplication extends GBRestletApplication {

    private static final Logger LOG = LoggerFactory.getLogger(RestApplication.class);
    private static Validator validator;
    private static DBI dbi;
    private Properties serviceProperties;

    /**
     * @return gets d current appl
     */
    public static RestApplication getCurrent() {
        return (RestApplication) Application.getCurrent();
    }

    /**
     * get DBI from Application.getCurrent().
     * @return handle to DBI object
     */
    public static DBI getDBICurrentApplication() {
        LOG.debug(DBConstants.DATA_SOURCE_NAME);
        return RestApplication.getCurrent().getDataSourceRegistry().getMonitoredDBI(DBConstants.DATA_SOURCE_NAME);
    }

    /*
     * @return
     * @see org.restlet.Application#createInboundRoot()
     */
    @Override
    public synchronized Router createInboundRoot() {

        Router router = super.createInboundRoot();
        String resourceUrl = null;
        this.getMetadataService().setDefaultMediaType(MediaType.APPLICATION_JSON);

        // resourceUrl = ResourcePath.getRidCountResourceRoute();//
        // Constants.NAMESPACE_ID_RESOURCE_ROUTE_TAG);

        resourceUrl = ResourcePathUrls.loginAuthentication();
        router.attach(resourceUrl, LoginASR.class);

        LOG.debug("Attaching hello-world with " + resourceUrl);

        resourceUrl = ResourcePathUrls.getEmployeeInfo(com.gwynniebee.backoffice.objects.Constants.UUID);
        router.attach(resourceUrl, EmployeePersonalDetails.class);

        resourceUrl = ResourcePathUrls.getManageEmployee();
        router.attach(resourceUrl, EmployeeManage.class);

        resourceUrl = ResourcePathUrls.employeeExists(com.gwynniebee.backoffice.objects.Constants.UUID);
        router.attach(resourceUrl, EmployeeExists.class);

        resourceUrl = ResourcePathUrls.employeeLeavesUrl(com.gwynniebee.backoffice.objects.Constants.SMALL_UUID);
        router.attach(resourceUrl, LeavesASR.class);

        resourceUrl = ResourcePathUrls.leavesUrl();
        router.attach(resourceUrl, AttendanceUploadASR.class);

        resourceUrl = ResourcePathUrls.attendanceTask();
        router.attach(resourceUrl, AsyncAttendanceUpdateASR.class);

        resourceUrl = ResourcePathUrls.notification();
        router.attach(resourceUrl, GenerateNotificationASR.class);

        resourceUrl = ResourcePathUrls.adminEmails();
        router.attach(resourceUrl, AdminEmailsForTeam1.class);

        // router.attach("/employee-portal/updateQuery",updatedetailsASR.class);
        LOG.debug("Attaching hello-world with " + resourceUrl);

        return router;
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#start()
     */
    @Override
    public synchronized void start() throws Exception {
        LOG.debug("START RESTLET");
        if (!this.isStarted()) {
            RestApplication.getCurrent().getDataSourceRegistry().getDBI(DBConstants.DATA_SOURCE_NAME);
            // RestApplication.getCurrent().getDataSourceRegistry().getDBI(DBConstants.DATA_SOURCE_NAME);
            this.serviceProperties = IOGBUtils.getPropertiesFromResource("/emp_prop.properties");
            LOG.debug(this.serviceProperties.getProperty(DBConstants.USERNAME) + this.serviceProperties.getProperty(DBConstants.DRIVER));
            LOG.debug(this.serviceProperties.getProperty(DBConstants.PASSWORD) + this.serviceProperties.getProperty("bucket"));
            LOG.debug(this.serviceProperties.getProperty(DBConstants.URL));
            BaseEntityManager.setDbi(getDBICurrentApplication());
            DBConstants.setBucket(this.serviceProperties.getProperty("bucket"));

        }
        // below will make this.isStarted() true
        super.start();
    }

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#stop()
     */
    @Override
    public synchronized void stop() throws Exception {
        if (!this.isStopped()) {
            LOG.info("Application.stop()");
        }
        // below will make this.isStopped() true
        super.stop();
    }

    /**
     * @return the serviceProps
     */
    public Properties getServiceProperties() {
        return this.serviceProperties;
    }

    /**
     * @return validator
     */
    public static Validator getValidator() {
        return validator;
    }

    /**
     * @param dbi settig for TESTCASES
     */
    public static void setDbi(DBI dbi) {
        RestApplication.dbi = dbi;
    }

    /**
     * @return DBI
     */
    public static DBI getDbi() {
        // TODO Auto-generated method stub
        return dbi;
    }
}
