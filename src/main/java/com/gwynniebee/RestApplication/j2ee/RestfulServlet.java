/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.RestApplication.j2ee;

import com.gwynniebee.RestApplication.restlet.RestApplication;
import com.gwynniebee.rest.service.j2ee.GBRestletServlet;

/**
 * @author sarath
 */
@SuppressWarnings("serial")
public class RestfulServlet extends GBRestletServlet<RestApplication> {
    /**
     * This is called by tomcat's start of servlet.
     */
    public RestfulServlet() {
        this(new RestApplication());
    }

    /**
     * This is called by unit test which create a subclass of this class with subclass of application.
     * @param app application
     */
    public RestfulServlet(RestApplication app) {
        super(app);
    }
}
