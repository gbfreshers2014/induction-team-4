/**
 * Copyright 2014 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

/**
 * @author harsh
 */
public interface AdminEmailsDao extends Transactional<AdminEmailsDao> {

    /**
     * @return list of uuids
     */
    @SqlQuery("select uuid from login_credentials where employee_role='admin'")
    List<String> getAdminuuidList();

    /**
     * @param uuid to be set
     * @return email_id
     */
    @SqlQuery("select email_id from employee_personal_details where uuid=:uuid")
    String getAdminEmailId(@Bind("uuid") String uuid);

}
