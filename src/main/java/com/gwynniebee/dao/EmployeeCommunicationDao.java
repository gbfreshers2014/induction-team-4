/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;


import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.CommunicationDetailsObject;
import com.gwynniebee.mappers.EmployeeCommunincationMapper;

/**
 * Database Employee Communication Details access DAO.
 * @author skishore
 */
@RegisterMapper(EmployeeCommunincationMapper.class)
public interface EmployeeCommunicationDao extends Transactional<EmployeeCommunicationDao> {

    /**
     * @param uuid of user
     * @return list of comm details
     */
    @SqlQuery("select * from employee_communication_details where uuid=:uuid")
    List<CommunicationDetailsObject> sqlquerycommunication(@Bind("uuid") String uuid);
}
