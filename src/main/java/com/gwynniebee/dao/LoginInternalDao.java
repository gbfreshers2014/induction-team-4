/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.mappers.LoginInternalMapper;

/**
 * Database personal details access for login DAO.
 * @author skishore
 */
@RegisterMapper(LoginInternalMapper.class)
public interface LoginInternalDao extends Transactional<LoginInternalDao> {
    /**
     * @param email id of user
     * @return uuid
     */
    @SqlQuery("select uuid from employee_personal_details where email_id=:email and employement_status!='retired'")
    String fetchuuid(@Bind("email") String email);
}
