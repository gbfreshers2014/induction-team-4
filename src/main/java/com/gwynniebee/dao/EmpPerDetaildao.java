/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;
import org.slf4j.Logger;

import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.LoginCred;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.mappers.EmpAddressDetailsMapper;
import com.gwynniebee.mappers.EmpCommunicationDetailsMapper;
import com.gwynniebee.mappers.EmpFamilyDetailsMapper;
import com.gwynniebee.mappers.EmpPersonalDetailsMapper;
import com.gwynniebee.mappers.LoginDetailsMapper;
//import org.skife.jdbi.v2.sqlobject.BindBean;
//import org.skife.jdbi.v2.sqlobject.BindBean;
//import org.skife.jdbi.v2.sqlobject.BindBean;

/**
 * @author rahul
 */
public interface EmpPerDetaildao extends Transactional<EmpPerDetaildao> {

    Logger LOG = org.slf4j.LoggerFactory.getLogger(EmpPerDetaildao.class);

    /**
     * @param ud takes the uuid of the user as the input.
     * @return return the personal details of the user.
     */
    @RegisterMapper(EmpPersonalDetailsMapper.class)
    @SqlQuery("select * from employee_personal_details where uuid=:ud")
    PersonalDetails getPersonalData(@Bind("ud") String ud);

    /**
     * @param ud takes the uuid of the user as the input.
     * @return return the login details of the user.
     */
    @RegisterMapper(LoginDetailsMapper.class)
    @SqlQuery("select * from login_credentials where uuid=:ud")
    LoginCred getLoginData(@Bind("ud") String ud);

    /**
     * @param ud takes the uuid of the user as the input.
     * @return return the Family details of the user.
     */
    @RegisterMapper(EmpFamilyDetailsMapper.class)
    @SqlQuery("select * from employee_family_details where uuid=:ud")
    List<FamilyDetails> getFamilyData(@Bind("ud") String ud);

    /**
     * @param ud takes the uuid of the user as the input.
     * @return return the Communnication details of the user.
     */
    @RegisterMapper(EmpCommunicationDetailsMapper.class)
    @SqlQuery("select * from employee_communication_details where uuid=:ud")
    List<CommunicationDetails> getCommunicationData(@Bind("ud") String ud);

    /**
     * @param ud takes the uuid of the user as the input.
     * @return return the address details of the user.
     */
    @RegisterMapper(EmpAddressDetailsMapper.class)
    @SqlQuery("select * from employee_address_details where uuid=:ud")
    List<AddressDetails> getAddressData(@Bind("ud") String ud);

    // Update profiles starts here.........................................

    // /**
    // * @param uuid takes the uuid of the user.
    // * @param createdOn when the profile was created
    // * @param createdBy by whom the profile was created.
    // * @param id id of the user who is creating new user.
    // * @param details takes communication details of the user.
    // * @param type type of communication details.
    // * @return return whether query run successfully.
    // */
    // @SqlUpdate("INSERT INTO employee_communication_details " +
    // "(details,last_updated_by,uuid,communication_type,created_by,created_on) "
    // + "VALUES (:details,:last_updated_by,:uuid,:type,:createdBy,NOW()) ")

    /**
     * @param uuid takes the uuid of the user.
     * @param details takes communication details of the user.
     * @param type type of communication details.
     * @param id id of the user who is creating new user.
     * @return returns the number of rows affected.
     */
    @SqlUpdate("UPDATE employee_communication_details SET details=:details,"
            + "last_updated_by=:last_updated_by WHERE uuid=:uuid and communication_type=:type ")
    int updateCommuDetails(@Bind("uuid") String uuid, @Bind("details") String details, @Bind("type") String type,
            @Bind("last_updated_by") String id);

    /**
     * @param uuid takes the uuid of the user.
     * @param updatingUuid id of the user who is updating details.
     * @param obj communication details object.
     * @return returns the number of rows affected.
     */
    @SqlUpdate("INSERT INTO employee_communication_details (uuid,communication_type,details,"
            + "created_on,created_by,last_updated_by) VALUES (:uuid,:obj.type,:obj.details,NOW()," + "last_updated_by,"
            + "last_updated_by) ")
    int updateNewCommuDetails(@Bind("uuid") String uuid, @Bind("last_updated_by") String updatingUuid,
            @BindBean("obj") CommunicationDetails obj);

    /**
     * @param uuid uuid of the user
     * @return numbers of rows affected.
     */
    @SqlUpdate("delete from employee_communication_details where uuid=:uuid")
    int deleteCommuDetails(@Bind("uuid") String uuid);

    // /**
    // * @param uuid uuid of the user.
    // * @param lastUpdatedBy uuid of the user who is updating the info.
    // * @param obj family details object.
    // * @param createdBy uuid of the user who created the details.
    // * @param createdOn date-time when the details were created.
    // * @return returns the number of rows affected.
    // */
    // @SqlUpdate("INSERT INTO employee_family_details ( blood_group,dependent,dob,relation,uuid "
    // + ",full_name,created_by,created_on,last_updated_by)"
    // +
    // " VALUES (:obj.bloodGroup, :obj.dependent,:obj.dob,:obj.relation,:uuid ,:obj.fullName,:createdBy,NOW(),:lastUpdatedBy)")
    //

    /**
     * @param uuid uuid of the user.
     * @param lastUpdatedBy id of the user who is updating inof.
     * @param obj obj of family details.
     * @return returns whether query run successfully.
     */
    @SqlUpdate("UPDATE employee_family_details SET blood_group=:obj.bloodGroup,dependent=:obj.dependent"
            + ",dob=:obj.dob,relation=:obj.relation,full_name=:obj.fullName,last_updated_by=:lastUpdatedBy"
            + " WHERE uuid=:uuid and dep_id=:obj.dependentID")
    int updateFamDetails(@Bind("uuid") String uuid, @Bind("lastUpdatedBy") String lastUpdatedBy, @BindBean("obj") FamilyDetails obj);

    /**
     * @param uuid uuid of the user.
     * @param lastUpdatedBy id of the user who is updating inof.
     * @param obj obj of family details.
     * @return returns whether query run successfully.
     */
    @SqlUpdate("INSERT INTO employee_family_details ( blood_group,dependent,dob,relation,uuid "
            + ",full_name,created_by,created_on,last_updated_by,dep_id)" + " VALUES (:obj.bloodGroup, :obj.dependent,:obj.dob,"
            + ":obj.relation,:uuid ,:obj.fullName,:lastUpdatedBy,NOW(),:lastUpdatedBy,:obj.dependentID)")
    int updateAddFamDetails(@Bind("uuid") String uuid, @Bind("lastUpdatedBy") String lastUpdatedBy, @BindBean("obj") FamilyDetails obj);

    /**
     * @param uuid uuid of the user
     * @return numbers of rows affected.
     */
    @SqlUpdate("delete from employee_family_details where uuid=:uuid")
    int deleteFamDetails(@Bind("uuid") String uuid);

    // /**
    // * @param uuid uuid of the user.
    // * @param id id of the user who is creating new user.
    // * @param createdOn date-time, when address details was created.
    // * @param country where user lives.
    // * @param createdBy id of the user who created the profile.
    // * @param streetAddress where user lives
    // * @param obj obj of address details.
    // * @return return whether query run successfully.
    // */
    // @SqlUpdate("INSERT INTO employee_address_details (city,country_code,state,"
    // +
    // "street_address,zip_code,last_updated_by,uuid,address_type,created_by,created_on ) VALUES"
    // + " (:obj.city,:country,:obj.state,:streetAdd,:obj.zipcode, " +
    // ":last_updated_by,:uuid,:obj.type,:createdBy,NOW())")
    /**
     * @param uuid uuid of the user.
     * @param lastUpdatedBy id of the user who is updating inof.
     * @param obj obj of address details.
     * @return returns whether query run successfully.
     */
    @SqlUpdate("UPDATE employee_address_details SET street_address=:obj.streetAddress, "
            + "city=:obj.city, state=:obj.state,country_code=:obj.country" + ",zip_code=:obj.zipcode,last_updated_by=:lastUpdatedBy "
            + "WHERE uuid=:uuid and address_type=:obj.type ")
    int updateAddressDetails(@Bind("uuid") String uuid, @Bind("lastUpdatedBy") String lastUpdatedBy, @BindBean("obj") AddressDetails obj);

    /**
     * @param id uuid of the user.
     * @param lastUpdatedBy id of the user who is updating inof.
     * @param obj obj of address details.
     * @return returns whether query run successfully.
     */
    @SqlUpdate("INSERT INTO employee_address_details (address_type,uuid,street_address,city,state,"
            + "country_code,zip_code,last_updated_by,created_by,created_on) " + "VALUES (:obj.type,:uuid,:obj.streetAddress,:obj.city,"
            + ":obj.state,:obj.country,:obj.zipcode,:lastUpdatedBy,:lastUpdatedBy,NOW())")
    int updateNewAddressDetails(@Bind("uuid") String id, @Bind("lastUpdatedBy") String lastUpdatedBy, @BindBean("obj") AddressDetails obj);

    /**
     * @param uuid uuid of the user
     * @return numbers of rows affected.
     */
    @SqlUpdate("delete from employee_address_details where uuid=:uuid")
    int deleteAddressDetails(@Bind("uuid") String uuid);

    /**
     * @param perDetails takes the personal details of the user.
     * @param id id of the user who is creating new user.
     * @param uuid uuid of the user.
     * @return return the number of rows affected.
     */
    @SqlUpdate("UPDATE employee_personal_details SET designation=:perDetails.designation, " + "emp_blood_group=:perDetails.bloodGroup, "
            + "emp_id=:perDetails.employeeId, first_name=:perDetails.firstName, " + "last_name=:perDetails.lastName, "
            + "dob=:perDetails.dob, doj=:perDetails.doj, last_updated_by=:lastUpdated, "
            + "employement_status=:perDetails.employmentStatus WHERE uuid=:uuid")
    int updatePersoDetails(@BindBean("perDetails") PersonalDetails perDetails, @Bind("uuid") String uuid, @Bind("lastUpdated") String id);

    /**
     * @param uuid uuid of the user.
     * @param password password of the user
     * @param lastUpdatedBy user who is updating the profile.
     * @param role role of the user.
     * @return returns the number of rows affected by the query.
     */
    @SqlUpdate("UPDATE login_credentials SET employee_role=:role, password=:pass, last_updated_by=:lastUpdated WHERE uuid=:uuid")
    int updateLoginCred(@Bind("uuid") String uuid, @Bind("pass") String password, @Bind("role") String role,
            @Bind("lastUpdated") String lastUpdatedBy);

    // int updatePersoDetails(@BindBean("perDetails") PersonalDetails
    // perDetails, @Bind("uuid") String uuid, @Bind("DOB") String dob,
    // @Bind("DOJ") String doj, @Bind("employeeId") String employeeId,
    // @Bind("employmentStatus") String employmentStatus,
    // @Bind("firstName") String firstName, @Bind("lastName") String lastName,
    // @Bind("bloodGroup") String bloodG,
    // @Bind("designation") String designation);

    //
    //
    // Add new employee details starts
    // here..................................................................................
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    /**
     * @param perD personal details of the user.
     * @param uuid uuid of the user.
     * @param id id of the user who is creating new user.
     * @return rows affectd by the query.
     */
    // @SqlUpdate("INSERT INTO employee_personal_details "
    // +
    // " (designation,emp_blood_group,emp_id,first_name,last_name,dob,doj,employement_status,uuid,email_id,last_updated_by)"
    // +
    // " VALUES (:(perDetails.getDesignation()),:(perDetails.getBloodGroup()), "
    // + ":(perDetails.getEmployeeId()), :(perDetails.getFirstName())," +
    // " :(perDetails.getLastName()), :(perDetails.getDob()), "
    // +
    // ":(perDetails.getDoj()), :(perDetails.getEmploymentStatus()),:uuid,:(perDetails.getEmail()), :last_updated_by)")
    @SqlUpdate("INSERT INTO employee_personal_details " + " (designation,emp_blood_group,emp_id,first_name,"
            + "last_name,dob,doj,employement_status,uuid,email_id,last_updated_by,created_by,created_on)"
            + " VALUES (:perDetails.designation,:perDetails.bloodGroup, " + "" + ":perDetails.employeeId, :perDetails.firstName,"
            + " :perDetails.lastName, :perDetails.dob, "
            + ":perDetails.doj, :perDetails.employmentStatus,:uuid,:perDetails.email, :last_updated_by,:last_updated_by, NOW())")
    int addPersoDetails(@BindBean("perDetails") PersonalDetails perD, @Bind("uuid") String uuid, @Bind("last_updated_by") String id);

    // @Bind("DOB") String dob, @Bind("DOJ") String doj, @Bind("employeeId")
    // String employeeId,
    // @Bind("employmentStatus") String employmentStatus, @Bind("firstName")
    // String firstName, @Bind("lastName") String lastName,
    // @Bind("bloodGroup") String bloodG, @Bind("designation") String
    // designation, @Bind("email") String email);

    /**
     * @param uuid takes the uuid of the user.
     * @param id id of the user who is creating the profile.
     * @param details takes communication details of the user.
     * @param type type of communication details.
     * @return return whether query run successfully.
     */
    @SqlUpdate("INSERT INTO employee_communication_details (details,uuid,communication_type,last_updated_by,"
            + "last_updated_on,created_by,created_on)" + " VALUES (:details,:uuid,:type, :last_updated_by,NOW(),"
            + ":last_updated_by,NOW())")
    int addCommuDetails(@Bind("uuid") String uuid, @Bind("details") String details, @Bind("type") String type,
            @Bind("last_updated_by") String id);

    /**
     * @param uuid uuid of the user.
     * @param obj family details object.
     * @param updatingId uuid of the user who is updating the family details.
     * @return returns whether query run successfully.
     */
    @SqlUpdate("INSERT INTO employee_family_details ( blood_group,dependent,dob,relation,"
            + "uuid, full_name,last_updated_by,created_by,created_on,dep_id)"
            + " VALUES  (:obj.bloodGroup,:obj.dependent,:obj.dob,:obj.relation,:uuid "
            + ",:obj.fullName,:updatingId,:updatingId,NOW(), :obj.dependentID)")
    int addFamDetails(@Bind("uuid") String uuid, @BindBean("obj") FamilyDetails obj, @Bind("updatingId") String updatingId);

    /**
     * @param uuid uuid of the user.
     * @param id id of the user who is adding new profile.
     * @param city city where user lives.
     * @param country where user lives.
     * @param state where user lives.
     * @param streetAddress where user lives
     * @param obj obj of address details.
     * @return return whether query run successfully.
     */
    @SqlUpdate("INSERT INTO employee_address_details (city,country_code,state,street_address,"
            + "zip_code,uuid,address_type,last_updated_by,last_updated_on,created_by,created_on) VALUES"
            + " (:city,:country,:state,:streetAdd,:obj.zipcode,:uuid,:obj.type," + ":last_updated_by,NOW(),:last_updated_by,NOW())")
    int addAddressDetails(@Bind("uuid") String uuid, @Bind("city") String city, @Bind("country") String country,
            @Bind("state") String state, @Bind("streetAdd") String streetAddress, @BindBean("obj") AddressDetails obj,
            @Bind("last_updated_by") String id);

    /**
     * @param uuid uuid of the user.
     * @param id id of the user who is adding new profile.
     * @param password password of the user.
     * @param role role of the user.
     * @return number of rows affected.
     */
    @SqlUpdate("INSERT INTO login_credentials (employee_role,password,uuid,last_updated_by) VALUES "
            + "(:role,:pass,(select uuid from employee_personal_details where uuid=:uuid),:last_updated_by)")
    int addLoginDetails(@Bind("uuid") String uuid, @Bind("pass") String password, @Bind("role") String role,
            @Bind("last_updated_by") String id);

    //
    //
    // @SqlUpdate(":qury")
    // int updateTableValues(@Bind("qury") String qury);
    // @SqlUpdate("UPDATE employeePersonalDetails SET lastName=:lastName WHERE UUID=:uuid")
    // int updateTableValues(@Bind("lastName") String lastName, @Bind("uuid")
    // String uuid);
    //
    // @SqlUpdate("insert into employeePersonalDetails values
    // (:UUID,:employeeId,:firstName,:lastName,:DOB,:DOJ,:employmentStatus,
    // :created_on,:created_by,:last_updated_on,:last_updated_by)")
    // int addNewData(@Bind("created_by") String created_by, @Bind("created_on")
    // String created_on, @Bind("DOB") String DOB,
    // @Bind("DOJ") String DOJ, @Bind("employeeId") String employeeId,
    // @Bind("employmentStatus") String employmentStatus,
    // @Bind("firstName") String firstName, @Bind("last_updated_by") String
    // last_updated_by,
    // @Bind("last_updated_on") String last_updated_on, @Bind("lastName") String
    // lastName, @Bind("UUID") String UUID);

}
// @RegisterMapper(UpdateDetailsMapper.class)
//
