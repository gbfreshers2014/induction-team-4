/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import java.sql.Date;
import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.IndividualLeaveObject;
import com.gwynniebee.backoffice.objects.LeavesDetails;
import com.gwynniebee.mappers.LeavesMapper;

/**
 * Database Attendance Register access DAO.
 * @author skishore
 */

public interface LeavesDao extends Transactional<LeavesDao> {
    /**
     * @param uuid is input
     * @param startdate is input
     * @param enddate ins input
     * @return leaves list
     */
    @RegisterMapper(LeavesMapper.class)
    @SqlQuery("select attendance_date,attendance_status from "
            + "attendance_register where uuid=:uuid and attendance_date>:sd and attendance_date<:ed")
    List<LeavesDetails> queryleaves(@Bind("uuid") String uuid, @Bind("sd") Date startdate, @Bind("ed") Date enddate);

    /**
     * @param obj object of the IndividualLeaveObject
     * @return returns number of rows affected.
     */
    @SqlUpdate("UPDATE attendance_register SET last_updated_by=:obj.updatingUuid, "
            + "attendance_status=:obj.attendanceStatus WHERE uuid=:obj.uuid and attendance_date=:obj.date ")
    int updateLeaves(@BindBean("obj") IndividualLeaveObject obj);

    /**
     * Closes connection.
     */
    void close();

}
