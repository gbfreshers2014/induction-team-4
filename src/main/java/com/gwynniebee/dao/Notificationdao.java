/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import java.sql.Date;
import java.util.List;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.mappers.NotificationMapper;

/**
 * DAO for attendance Task used for Notification.
 * @author skishore
 */
@RegisterMapper(NotificationMapper.class)
public interface Notificationdao extends Transactional<Notificationdao> {

    /**
     * @return pending action to be perfomed by admin
     */
    @SqlQuery("select attendance_date from attendance_task where attendance_action='not-performed'")
    List<Date> getpendingattendancedates();

}
