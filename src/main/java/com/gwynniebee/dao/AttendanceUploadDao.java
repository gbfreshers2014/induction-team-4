/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import java.sql.Date;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.mappers.GetUuidMapper;

/**
 * Database Access Attendance DAO.
 * @author skishore
 */
public interface AttendanceUploadDao extends Transactional<AttendanceUploadDao> {
    /**
     * @param uuid of user
     * @param status absent presnet
     * @param adminuuid uuid of changer
     * @param date of the date
     * @return status of transaction
     */
    @SqlUpdate("insert into attendance_register(uuid,attendance_date,attendance_status,created_by,"
            + "last_updated_by,created_on,last_updated_on) values(:uuid,:da" + "te,:status,:adminuuid,:adminuuid,NOW(),NOW())")
    int updateattendance(@Bind("uuid") String uuid, @Bind("status") String status, @Bind("date") Date date,
            @Bind("adminuuid") String adminuuid);

    /**
     * @param emailId email id of the users.
     * @return return the response of select query.
     */
    @RegisterMapper(GetUuidMapper.class)
    @SqlQuery("select uuid from employee_personal_details WHERE email_id=:emailId")
    String extractUuid(@Bind("emailId") String emailId);
}
