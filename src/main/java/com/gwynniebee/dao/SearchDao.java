/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.SearchResult;
import com.gwynniebee.mappers.SearchMapper;

/**
 * Database employee personal details access for search access DAO.
 * @author skishore
 */
@RegisterMapper(SearchMapper.class)
public interface SearchDao extends Transactional<SearchDao> {

    /**
     * @param param input string
     * @return list of results
     */
    @SqlQuery("select * from employee_personal_details where (uuid like :para OR first_name like :para OR "
            + "last_name like :para OR email_id like :para)")
    List<SearchResult> sqlquery(@Bind("para") String param);

    /**
     * @return all employees
     */
    @SqlQuery("select * from employee_personal_details")
    List<SearchResult> queryall();

}
