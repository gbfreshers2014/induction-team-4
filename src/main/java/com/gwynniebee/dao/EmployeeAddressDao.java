/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;


import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.AddressDetailsObject;
import com.gwynniebee.mappers.EmployeeAddressMapper;

/**
 * Database Employee Address access DAO.
 * @author skishore
 */
@RegisterMapper(EmployeeAddressMapper.class)
public interface EmployeeAddressDao extends Transactional<EmployeeAddressDao> {

    /**
     * @param uuid of user
     * @return  list of addressses
     */
    @SqlQuery("select * from employee_address_details where uuid=:uuid")
    List<AddressDetailsObject> sqlqueryaddress(@Bind("uuid") String uuid);
}
