/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import java.sql.Date;
import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.Taskdetails;
import com.gwynniebee.mappers.TaskMapper;

/**
 * Database attendance_tasks access for search access DAO.
 * @author skishore
 */
@RegisterMapper(TaskMapper.class)
public interface Taskdao extends Transactional<Taskdao> {

    /**
     * @return list of tasks to be done by Async
     * @param date check date
     */
    @SqlQuery("select * from attendance_task where attendance_date=:date")
    Taskdetails checktask(@Bind("date") Date date);

    /**
     * @return list of tasks to be done by Async
     */
    @SqlQuery("select * from attendance_task where async_status=0 and attendance_action != 'not-performed'")
    List<Taskdetails> gettasks();

    /**
     * @return list of tasks to be done by Admin
     */
    @SqlQuery("select * from attendance_task where attendance_action='not-performed' and attendance_date<curdate()")
    List<Taskdetails> getadmintasks();

    /**
     * @param date date of task
     * @param action allpresent/holiday/absent
     * @param createdby updationguuid
     * @return succss/failure
     */
    @SqlUpdate("insert into attendance_task values(:date,0,:action,:createdby,NOW())")
    int updatetasks(@Bind("date") java.sql.Date date, @Bind("action") String action, @Bind("createdby") String createdby);

    /**
     * @param date of the date to be set
     * @return success
     */
    @SqlUpdate("insert into attendance_task values(:date,0,'not-performed',0,'0000-00-00')")
    int dailyinsert(@Bind("date") Date date);

    /**
     * @param date accomplished date
     * @return success/failure
     */
    @SqlUpdate("update attendance_task set async_status=1 where attendance_date=:date")
    int removetask(@Bind("date") Date date);

    /**
     * @param date date of task
     * @param action allpresent/holiday/absent
     * @param createdby updationguuid
     * @return succss/failure
     */
    @SqlUpdate("update attendance_task set attendance_action=:action,async_status=0"
            + ",action_done_by=:createdby,action_done_on=NOW() where attendance_date=:date")
    int updateexistingtasks(@Bind("date") java.sql.Date date, @Bind("action") String action, @Bind("createdby") String createdby);

}
