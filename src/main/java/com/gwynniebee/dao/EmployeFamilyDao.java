/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.FamilyDetailsObject;
import com.gwynniebee.mappers.EmployeeFamilyMapper;

/**
 * Database Employee Family access DAO.
 * @author skishore
 */
@RegisterMapper(EmployeeFamilyMapper.class)
public interface EmployeFamilyDao extends Transactional<EmployeFamilyDao> {
    /**
     * @param uuid of user
     * @return list of employee family details
     */
    @SqlQuery("select * from employee_family_details where uuid=:uuid")
    List<FamilyDetailsObject> sqlqueryfamily(@Bind("uuid") String uuid);
}
