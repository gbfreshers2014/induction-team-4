/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

/**
 * sets employment status.
 * @author skishore
 */
public interface Deletedao extends Transactional<Deletedao> {

    /**
     * @param uuid uuid
     * @param adminuuid admin uuid
     * @return status
     */
    @SqlUpdate("Update employee_personal_details SET employement_status='retired',"
            + "last_updated_by=:adminuuid,last_updated_on=NOW() WHERE UUID=:UUID")
    int changeempstatus(@Bind("UUID") String uuid, @Bind("adminuuid") String adminuuid);

}
