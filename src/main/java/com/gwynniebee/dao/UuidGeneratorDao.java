/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;


import java.util.List;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.mappers.UuidGeneratorMapper;

/**
 * Database employee personal details for attendance Upload access DAO.
 * @author skishore
 */
@RegisterMapper(UuidGeneratorMapper.class)
public interface UuidGeneratorDao extends Transactional<UuidGeneratorDao> {
    /**
     * @return uuid
     */
    @SqlQuery("select uuid from employee_personal_details where employement_status!='retired'")
    List<String> generateUUID();
}
