/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.LoginResponseObject;
import com.gwynniebee.mappers.LoginMapper;

/**
 * Database login credentials access DAO.
 * @author skishore
 */
@RegisterMapper(LoginMapper.class)
public interface LoginDao extends Transactional<LoginDao> {

    /**
     * @param uuid of user
     * @param password of user
     * @return response auth/fail 0/1
     */
    @SqlQuery("select uuid,employee_role from login_credentials where uuid=:uuid and password=:password")
    LoginResponseObject authenticateuserandspecifyrole(@Bind("uuid") String uuid, @Bind("password") String password);
}
