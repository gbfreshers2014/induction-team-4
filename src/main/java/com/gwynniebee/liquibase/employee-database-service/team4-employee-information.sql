CREATE DATABASE IF NOT EXISTS `team4-employee-information`;
USE `team4-employee-information`;

CREATE TABLE `employee_personal_details` (
  `uuid` char(36) NOT NULL,
  `emp_id` char(5) NOT NULL,
  `first_name` varchar(35) NOT NULL,
  `last_name` varchar(35) NOT NULL,
  `email_id` varchar(254) NOT NULL,
  `dob` date NOT NULL,
  `doj` date NOT NULL,
  `employement_status` enum('employed','retired') NOT NULL,
  `emp_blood_group` char(3) NOT NULL,
  `designation` varchar(45) NOT NULL,
  `created_by` char(36) NOT NULL,
  `created_on` datetime NOT NULL,
  `last_updated_by` char(36) NOT NULL,
  `last_updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `email_id_UNIQUE` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `attendance_register`;

CREATE TABLE `attendance_register` (
  `uuid` char(36) NOT NULL,
  `date` date NOT NULL,
  `status` enum('absent','leave','present','holiday','unknown') NOT NULL,
  `created_by` char(36) NOT NULL,
  `created_on` datetime NOT NULL,
  `last_updated_by` char(36) NOT NULL,
  `last_updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`,`date`),
  CONSTRAINT `fk_attendance_register_1` FOREIGN KEY (`uuid`) REFERENCES `employee_personal_details` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `attendance_task`;

CREATE TABLE `attendance_task` (
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `action` enum('csv_uploaded','holiday','all_present') NOT NULL,
  `action_done_by` char(36) NOT NULL,
  `action_done_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `employee_address_details`;

CREATE TABLE `employee_address_details` (
  `uuid` char(36) NOT NULL,
  `type` enum('permanent','current') NOT NULL,
  `street_address` varchar(100) NOT NULL,
  `city` varchar(35) NOT NULL,
  `state` varchar(35) NOT NULL,
  `country_code` char(2) NOT NULL,
  `zip_code` char(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` char(36) NOT NULL,
  `last_updated_by` char(36) NOT NULL,
  `last_updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`,`type`),
  CONSTRAINT `fk_employee_address_details_1` FOREIGN KEY (`uuid`) REFERENCES `employee_personal_details` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `employee_communication_details`;

CREATE TABLE `employee_communication_details` (
  `uuid` char(36) NOT NULL,
  `type` enum('phone_no','skype_id','personal_email_id') NOT NULL,
  `details` varchar(254) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` char(36) NOT NULL,
  `last_updated_by` char(36) NOT NULL,
  `last_updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`,`type`),
  CONSTRAINT `fk_employee_communication_details_1` FOREIGN KEY (`uuid`) REFERENCES `employee_personal_details` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `employee_family_details`;

CREATE TABLE `employee_family_details` (
  `uuid` char(36) NOT NULL,
  `full_name` varchar(70) NOT NULL,
  `dependent` enum('true','false') NOT NULL,
  `dob` varchar(45) NOT NULL,
  `blood_group` varchar(45) NOT NULL,
  `relation` enum('father','mother','wife','child') NOT NULL,
  PRIMARY KEY (`uuid`,`full_name`),
  CONSTRAINT `fk_employee_family_details_1` FOREIGN KEY (`uuid`) REFERENCES `employee_personal_details` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `login_credentials`;

CREATE TABLE `login_credentials` (
  `uuid` char(36) NOT NULL,
  `password` char(64) NOT NULL,
  `last_updated_by` char(36) NOT NULL,
  `last_updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role` enum('admin','employee') NOT NULL,
  PRIMARY KEY (`uuid`),
  CONSTRAINT `fk_login_credentials_1` FOREIGN KEY (`uuid`) REFERENCES `employee_personal_details` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

USE `team4-employee-information`;
CREATE trigger before_insert_employee_personal_details BEFORE INSERT ON employee_personal_details
FOR EACH ROW  SET NEW.created_on = CURRENT_TIMESTAMP;

USE `team4-employee-information`;
CREATE trigger before_insert_attendance_register BEFORE INSERT ON attendance_register
FOR EACH ROW  SET NEW.created_on = CURRENT_TIMESTAMP;

USE `team4-employee-information`;
CREATE trigger before_insert_employee_address_details BEFORE INSERT ON employee_address_details
FOR EACH ROW  SET NEW.created_on = CURRENT_TIMESTAMP;

USE `team4-employee-information`;
CREATE trigger before_insert_employee_communication_details BEFORE INSERT ON employee_communication_details
FOR EACH ROW  SET NEW.created_on = CURRENT_TIMESTAMP;




