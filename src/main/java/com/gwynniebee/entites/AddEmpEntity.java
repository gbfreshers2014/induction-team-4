/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.LoginCred;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.backoffice.restlet.responses.Details;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.dao.EmpPerDetaildao;
import com.gwynniebee.rest.common.response.ResponseStatus;

//import com.gwynniebee.dao.logindao;
/**
 * @author rahul
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddEmpEntity {

    /**
     * @param obj detailed information of the user.
     * @return returns the response of the query.
     */
    public Generichandler addEmployee(Details obj) {
        Handle h = null;
        Generichandler toBeReturned = new Generichandler();
        ResponseStatus repst = new ResponseStatus();
        try {
            h = BaseEntityManager.getDBIh();
            EmpPerDetaildao dao = null;
            dao = h.attach(EmpPerDetaildao.class);
            EmployeeDetails temp = new EmployeeDetails();
            temp = obj.getEmployeeDetails();
            PersonalDetails perTemp = new PersonalDetails();
            perTemp = temp.getPersonalDetails();
            List<CommunicationDetails> commuTemp = new ArrayList<CommunicationDetails>();
            commuTemp = temp.getCommunicationDetails();
            List<FamilyDetails> famTemp = new ArrayList<FamilyDetails>();
            famTemp = temp.getFamilyDetails();
            List<AddressDetails> addressTemp = new ArrayList<AddressDetails>();
            addressTemp = temp.getAddressDetails();

            int output0 = 0;
            int output1 = 0;
            int output2 = 0;
            int output3 = 0;
            int output4 = 0;

            UUID uuid = UUID.nameUUIDFromBytes(perTemp.getEmail().getBytes());
            obj.setUuid(uuid.toString());
            toBeReturned.setUuid(uuid.toString());

            LoginCred loginCred = new LoginCred();
            loginCred = obj.getLoginCred();
            h.begin();
            output1 = dao.addPersoDetails(perTemp, obj.getUuid(), obj.getUpdatingUuid());
            output0 = dao.addLoginDetails(obj.getUuid(), loginCred.getPassword(), loginCred.getRole(), obj.getUpdatingUuid());

            // dao.addPersoDetails(obj.getUuid(), perTemp.dob, perTemp.doj,
            // perTemp.employeeId, perTemp.employmentStatus,
            //
            // perTemp.firstName, perTemp.lastName, perTemp.bloodGroup,
            // perTemp.designation, perTemp.email);

            for (CommunicationDetails tempObj : commuTemp) {
                output2 = dao.addCommuDetails(obj.getUuid(), tempObj.getDetails(), tempObj.getType(), obj.getUpdatingUuid());

            }

            int itr = 0;
            for (FamilyDetails tempObj : famTemp) {
                tempObj.setDependentID(String.valueOf(System.nanoTime()) + itr);
                itr++;
                output3 = dao.addFamDetails(obj.getUuid(), tempObj, obj.getUpdatingUuid());

            }

            for (AddressDetails tempObj : addressTemp) {
                output4 =
                        dao.addAddressDetails(obj.getUuid(), tempObj.getCity(), tempObj.getCountry(), tempObj.getState(),
                                tempObj.getStreetAddress(), tempObj, obj.getUpdatingUuid());

            }

            repst.setCode(0);
            repst.setMessage("updated_successfully");
            toBeReturned.setStatus(repst);
            h.commit();
            return toBeReturned;
        } catch (Exception e) {
            if (h != null) {
                h.rollback();
            }
            repst.setCode(1);
            repst.setMessage(e.getMessage());
            toBeReturned.setStatus(repst);
            return toBeReturned;
        } finally {
            toBeReturned.setStatus(repst);
            if (h != null) {
                h.close();
            }
        }

        // System.out.println(toBeReturned.employeeId);

    }
}
