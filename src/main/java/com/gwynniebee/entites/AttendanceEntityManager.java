/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.dao.AttendanceUploadDao;
import com.gwynniebee.dao.UuidGeneratorDao;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * Entity manager for Attendance Entity.
 * @author skishore
 */
public class AttendanceEntityManager {

    private static final Logger LOG = LoggerFactory.getLogger(AttendanceEntityManager.class);

    /**
     * @param emailIdSet is input
     * @param date is set
     * @param uploadinguuid is input
     * @return attendance response
     */
    public ResponseStatus updateAttendance(HashSet<String> emailIdSet, Date date, String uploadinguuid) {
        Handle h = null;
        ResponseStatus status = new ResponseStatus();
        try {
            h = BaseEntityManager.getDBIh();
            UuidGeneratorDao dao = h.attach(UuidGeneratorDao.class);
            AttendanceUploadDao atddao = h.attach(AttendanceUploadDao.class);
            h.begin();

            Iterator<String> iterator = emailIdSet.iterator();
            HashSet<String> uuidSet = new HashSet<String>();
            while (iterator.hasNext()) {
                uuidSet.add(atddao.extractUuid(iterator.next()));
            }

            ArrayList<String> uuidlist = (ArrayList<String>) dao.generateUUID();
            for (String uuid : uuidlist) {
                if (uuidSet.contains(uuid)) {
                    atddao.updateattendance(uuid, "absent", date, uploadinguuid);
                } else {
                    atddao.updateattendance(uuid, "present", date, uploadinguuid);
                }
            }
            h.commit();
            status.setCode(ResponseStatus.CODE_SUCCESS);
            status.setMessage("Successfully Updated");

        } catch (Exception e) {
            h.rollback();
            status.setCode(1);
            status.setMessage(e.getMessage());

        } finally {
            if (h != null) {
                h.close();
            }
        }
        return status;
    }

    /**
     * @param date updating
     * @param string present/holiday
     * @return result
     * @param uploadinguuid admin uuid
     * @throws ClassNotFoundException noideaa
     */
    public ResponseStatus updateAttendance(java.sql.Date date, String string, String uploadinguuid) throws ClassNotFoundException {
        Handle h = null;
        ResponseStatus status = new ResponseStatus();
        try {
            h = BaseEntityManager.getDBIh();
            UuidGeneratorDao dao = h.attach(UuidGeneratorDao.class);
            AttendanceUploadDao atddao = h.attach(AttendanceUploadDao.class);
            h.begin();
            ArrayList<String> uuidlist = (ArrayList<String>) dao.generateUUID();

            for (String uuid : uuidlist) {
                if (string.equals("processed")) {
                    atddao.updateattendance(uuid, "present", date, uploadinguuid);
                }
                if (string.equals("holiday")) {
                    atddao.updateattendance(uuid, "holiday", date, uploadinguuid);
                }
            }
            h.commit();
            status.setCode(ResponseStatus.CODE_SUCCESS);
            status.setMessage("Successfully Updated");
        } catch (Exception e) {
            status.setCode(1);
            status.setMessage("Not Updated");
            LOG.debug(e.getMessage());
        } finally {
            if (h != null) {
                h.close();
            }
        }
        return status;
    }
}
