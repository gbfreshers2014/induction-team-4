/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.AddressDetailsObject;
import com.gwynniebee.backoffice.objects.CommunicationDetailsObject;
import com.gwynniebee.backoffice.objects.FamilyDetailsObject;
import com.gwynniebee.backoffice.objects.SearchResult;
import com.gwynniebee.backoffice.objects.SearchResultArray;
import com.gwynniebee.backoffice.restlet.resources.EmployeeManage;
import com.gwynniebee.backoffice.restlet.responses.SearchHandler;
import com.gwynniebee.dao.EmployeFamilyDao;
import com.gwynniebee.dao.EmployeeAddressDao;
import com.gwynniebee.dao.EmployeeCommunicationDao;
import com.gwynniebee.dao.SearchDao;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * Entity manager for Search Entity.
 * @author skishore
 */
public class SearchEntityManager {
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeManage.class);

    /**
     * @param currentQueryParam parameters
     * @return search results
     * @throws ClassNotFoundException sql file not found
     */
    public SearchHandler searchQuery(String currentQueryParam) throws ClassNotFoundException {
        SearchDao dao = null;
        EmployeeAddressDao addrdao = null;
        EmployeFamilyDao famdao = null;
        EmployeeCommunicationDao cmddao = null;
        ResponseStatus repst = new ResponseStatus();
        SearchHandler handle = new SearchHandler();
        currentQueryParam = "%" + currentQueryParam + "%";
        try {
            dao = BaseEntityManager.getDbi().open(SearchDao.class);
            famdao = BaseEntityManager.getDbi().open(EmployeFamilyDao.class);
            addrdao = BaseEntityManager.getDbi().open(EmployeeAddressDao.class);
            cmddao = BaseEntityManager.getDbi().open(EmployeeCommunicationDao.class);
            SearchResultArray sra = new SearchResultArray();
            LOG.debug("about to call sql");
            sra.setEmployeeList((ArrayList<SearchResult>) dao.sqlquery(currentQueryParam));
            LOG.debug("after sql call");
            for (SearchResult sr : sra.getEmployeeList()) {
                LOG.debug("looping over employee" + sr.getPersonalDetails().getUuid());
                sr.setAdrressDetails((ArrayList<AddressDetailsObject>) addrdao.sqlqueryaddress(sr.getPersonalDetails().getUuid()));
                LOG.debug("got address" + sr.getAdrressDetails().size());
                sr.setCommunicatiDetails((ArrayList<CommunicationDetailsObject>) cmddao.sqlquerycommunication(sr.getPersonalDetails()
                        .getUuid()));
                LOG.debug("got comms" + sr.getCommunicatiDetails().size());
                sr.setFamilyDetails((ArrayList<FamilyDetailsObject>) famdao.sqlqueryfamily(sr.getPersonalDetails().getUuid()));
                LOG.debug("got family" + sr.getFamilyDetails().size());
            }
            LOG.debug("loop ended" + sra.getEmployeeList().size());
            ArrayList<SearchResult> temp = sra.getEmployeeList();
            handle.setList(temp);
            LOG.debug("check ended" + sra.getEmployeeList().size());
            repst.setCode(0);
            repst.setMessage("Successful");
            handle.setStatus(repst);
            LOG.debug("check 2 ended" + sra.getEmployeeList().size());
        } catch (Exception e) {
            repst.setCode(1);
            LOG.debug(e.getMessage());
            repst.setMessage(e.getMessage());
            handle.setStatus(repst);
        }
        return handle;
    }

    /**
     * @return all results
     * @throws ClassNotFoundException notfound
     */
    public SearchHandler retieveall() throws ClassNotFoundException {
        SearchDao dao = null;
        ResponseStatus repst = new ResponseStatus();
        SearchHandler handle = new SearchHandler();
        try {
            dao = BaseEntityManager.getDbi().open(SearchDao.class);
            SearchResultArray sra = new SearchResultArray();
            sra.setEmployeeList((ArrayList<SearchResult>) dao.queryall());
            handle.setList(sra.getEmployeeList());
            repst.setCode(0);
            repst.setMessage("Successful");
        } catch (Exception e) {
            repst.setCode(1);
            repst.setMessage(e.getMessage());
        }
        return handle;
    }
}
