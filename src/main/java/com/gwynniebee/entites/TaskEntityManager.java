/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import java.util.ArrayList;
import java.util.Calendar;

import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.Constants;
import com.gwynniebee.backoffice.objects.TaskObject;
import com.gwynniebee.backoffice.objects.Taskdetails;
import com.gwynniebee.dao.Taskdao;
import com.gwynniebee.rest.common.response.ResponseStatus;
//import org.eclipse.jetty.util.log.Log;
//import com.gwynniebee.backoffice.restlet.resources.AsyncAttendanceUpdateASR;

/**
 * Task Entity Manager.
 * @author skishore
 */
public class TaskEntityManager {
    private static final Logger LOG = LoggerFactory.getLogger(TaskEntityManager.class);

    /**
     * @return all the tasks not performed
     */
    public TaskObject gettasks() {
        TaskObject tobj = new TaskObject();
        try {
            Taskdao taskdao = BaseEntityManager.getDbi().open(Taskdao.class);
            ArrayList<Taskdetails> td = (ArrayList<Taskdetails>) taskdao.gettasks();
            tobj.setTask(td);
        } catch (Exception e) {
            LOG.debug(e.getMessage());
            tobj = null;
        }
        return tobj;
    }

    /**
     * @return all the tasks to be done by admin in notification
     */
    public TaskObject getadmintasks() {
        TaskObject tobj = new TaskObject();
        try {
            Taskdao taskdao = BaseEntityManager.getDbi().open(Taskdao.class);
            ArrayList<Taskdetails> td = (ArrayList<Taskdetails>) taskdao.getadmintasks();
            tobj.setTask(td);
        } catch (Exception e) {
            tobj = null;
            System.out.println("ERROR is " + e.getMessage());
        }
        return tobj;
    }

    /**
     * @param date new task date
     * @param action corresponding action
     * @param updatinguuid updating uuid
     * @return success/failuure
     */
    public ResponseStatus updatetasks(java.sql.Date date, String action, String updatinguuid) {
        Handle h = null;
        ResponseStatus repst = new ResponseStatus();
        try {
            h = BaseEntityManager.getDBIh();
            h.begin();
            Taskdao taskdao = h.attach(Taskdao.class);
            int code;
            Taskdetails temp = null;
            temp = taskdao.checktask(date);
            if (temp == null) {
                code = taskdao.updatetasks(date, action, updatinguuid);
            } else {
                code = taskdao.updateexistingtasks(date, action, updatinguuid);
            }
            h.commit();
            repst.setCode(ResponseStatus.CODE_SUCCESS);
            repst.setMessage("Successfully Updated");
        } catch (Exception e) {
            repst.setCode(1);
            repst.setMessage("Not Updated");
        } finally {
            h.close();
        }
        return repst;
    }

    /**
     * @param date date corresponding
     * @return success/failure
     * @throws ClassNotFoundException class not found
     */
    public ResponseStatus deletetask(java.sql.Date date) throws ClassNotFoundException {
        Handle h = null;
        ResponseStatus rs = new ResponseStatus();
        try {
            h = BaseEntityManager.getDBIh();
            h.begin();
            Taskdao taskdao = h.attach(Taskdao.class);
            int code = taskdao.removetask(date);
            h.commit();
            rs.setCode(ResponseStatus.CODE_SUCCESS);
            rs.setMessage("Successful");
        } catch (Exception e) {
            rs.setCode(ResponseStatus.CODE_SUCCESS);
            rs.setMessage(e.getMessage());
        } finally {
            if (h != null) {
                h.close();
            }
        }
        return rs;
    }

    /**
     * @param date date corresponding
     * @return success/failure
     * @throws ClassNotFoundException class not found
     */
    public ResponseStatus dailyinsert(java.sql.Date date) throws ClassNotFoundException {
        Handle h = null;
        ResponseStatus repst = new ResponseStatus();
        try {
            h = BaseEntityManager.getDBIh();
            h.begin();
            Taskdao taskdao = h.attach(Taskdao.class);
            int code = 1;
            Taskdetails temp = null;
            for (int i = 0; i < Constants.LOOKUP; i++) {
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                c.add(Calendar.DATE, -i);
                java.sql.Date itdate = new java.sql.Date(c.getTime().getTime());
                temp = taskdao.checktask(itdate);
                if (temp == null) {
                    code = taskdao.dailyinsert(itdate);
                }
            }
            repst.setCode(ResponseStatus.CODE_SUCCESS);
            repst.setMessage("Successful");
            h.commit();
        } catch (Exception e) {
            h.rollback();
            repst.setCode(1);
            repst.setMessage(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            if (h != null) {
                h.close();
            }
        }
        return repst;
    }
}
