/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.DeleteObject;
import com.gwynniebee.dao.Deletedao;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * Entity manager for Delete Entity.
 * @author vikas,skishore
 */
public class DeleteEntityManager {

    private static final Logger LOG = LoggerFactory.getLogger(DeleteEntityManager.class);

    /**
     * @param deleteinst input uuid and uploaduuid
     * @return status
     * @throws ClassNotFoundException classexception
     */
    public ResponseStatus changeemployemntstatus(DeleteObject deleteinst) throws ClassNotFoundException {
        Handle h = null;
        ResponseStatus status = new ResponseStatus();
        try {
            h = BaseEntityManager.getDBIh();
            Deletedao dao = null;
            h.begin();
            dao = h.attach(Deletedao.class);
            LOG.debug("UPDATIONG UUID:" + deleteinst.getUpdatinguuid());
            int success = dao.changeempstatus(deleteinst.getUUID(), deleteinst.getUpdatinguuid());
            h.commit();
            status.setCode(0);
            status.setMessage("successfully updated");

        } catch (Exception e) {
            h.rollback();
            status.setCode(1);
            status.setMessage(e.getMessage());
        } finally {
            if (h != null) {
                h.close();
            }
        }
        return status;
    }
}
