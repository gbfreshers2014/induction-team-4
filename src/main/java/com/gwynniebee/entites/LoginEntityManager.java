/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.LoginObject;
import com.gwynniebee.backoffice.objects.LoginResponseObject;
import com.gwynniebee.backoffice.restlet.responses.LoginHandler;
import com.gwynniebee.dao.LoginDao;
import com.gwynniebee.dao.LoginInternalDao;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * Entity manager for Login Entity.
 * @author skishore
 */
public class LoginEntityManager {
    private static final Logger LOG = LoggerFactory.getLogger(LoginEntityManager.class);

    /**
     * @param logininst input from user
     * @return response of login
     * @throws ClassNotFoundException notfound
     */
    public LoginHandler authenticate(LoginObject logininst) throws ClassNotFoundException {
        LOG.debug("IN LOGIN INST");
        LoginDao dao = null;
        LoginInternalDao idao = null;
        ResponseStatus repst = new ResponseStatus();
        LoginHandler lh = new LoginHandler();
        LoginResponseObject tobereturned = null;
        try {
            idao = BaseEntityManager.getDbi().open(LoginInternalDao.class);
            dao = BaseEntityManager.getDbi().open(LoginDao.class);
            String uuid = idao.fetchuuid(logininst.getEmailid());
            LOG.debug("UUID:" + uuid);

            if (uuid != null && uuid.length() > 0) {
                tobereturned = dao.authenticateuserandspecifyrole(uuid, logininst.getPassword());
            }
            LOG.debug("Here before role and uuid fetch");
            repst.setCode(ResponseStatus.CODE_SUCCESS);
            repst.setMessage("Successfully Authenticated");
            lh.setRole(tobereturned.getRole());
            lh.setUuid(tobereturned.getUUID());
        } catch (Exception e) {
            repst.setCode(1);
            repst.setMessage(e.getMessage());
        } finally {
            lh.setStatus(repst);
        }
        // LOG.debug(tobereturned.role + tobereturned.uuid);
        return lh;
    }
}
