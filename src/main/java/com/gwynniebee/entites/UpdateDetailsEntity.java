/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;

import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.LoginCred;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.backoffice.restlet.responses.Details;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.dao.EmpPerDetaildao;
import com.gwynniebee.rest.common.response.ResponseStatus;

//import com.gwynniebee.dao.logindao;
/**
 * @author rahul
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateDetailsEntity {

    private final Logger log = org.slf4j.LoggerFactory.getLogger(UpdateDetailsEntity.class);

    // public static void main(String[] a) throws ClassNotFoundException {
    // UpdateDetailsEntity aaa = new UpdateDetailsEntity();
    // UpdateDetailsEntity obj = new UpdateDetailsEntity();
    // Details objj = new Details();
    // aaa.updateDetails(objj);
    //
    // }

    /**
     * @param obj takes the object of details class, which contain the all
     *            information of the user.
     * @return return the response of the query.
     * @throws ClassNotFoundException throws exception if any used class is
     *             missing.
     */
    public Generichandler updateDetails(Details obj) throws ClassNotFoundException {

        Handle h = null;

        try {
            h = BaseEntityManager.getDBIh();
            EmpPerDetaildao dao = null;

            h.begin();
            dao = h.attach(EmpPerDetaildao.class);
            // // loginresponseobject tobereturned =

            Generichandler toBeReturned = new Generichandler();

            int output = 0;
            EmployeeDetails temp = new EmployeeDetails();

            temp = obj.getEmployeeDetails();
            PersonalDetails perTemp = new PersonalDetails();
            perTemp = temp.getPersonalDetails();

            // List<CommunicationDetails> commuTemp = new
            // ArrayList<CommunicationDetails>();
            // commuTemp = temp.getCommunicationDetails();

            List<FamilyDetails> famTemp = new ArrayList<FamilyDetails>();
            famTemp = temp.getFamilyDetails();

            List<AddressDetails> addressTemp = new ArrayList<AddressDetails>();
            addressTemp = temp.getAddressDetails();

            int output0 = 0;
            int output1 = 0;
            int output2 = 0;
            int output3 = 0;
            int output4 = 0;

            LoginCred loginCred = new LoginCred();
            loginCred = obj.getLoginCred();
            output0 = dao.updateLoginCred(obj.getUuid(), loginCred.getPassword(), loginCred.getRole(), obj.getUpdatingUuid());
            output1 = dao.updatePersoDetails(perTemp, obj.getUuid(), obj.getUpdatingUuid());
            // , perTemp.dob, perTemp.doj, perTemp.employeeId,
            // perTemp.employmentStatus, perTemp.firstName,
            // perTemp.lastName, perTemp.bloodGroup, perTemp.designation);
            //
            //
            //
            // /
            //
            //
            // Handling Updation in communication details.........

            List<CommunicationDetails> commuTemp = new ArrayList<CommunicationDetails>();
            commuTemp = temp.getCommunicationDetails();

            List<CommunicationDetails> tempForOldValues = new ArrayList<CommunicationDetails>();
            // Log.debug("here");
            // this.log.debug("hereeeee" + tempForOldValues);
            tempForOldValues = dao.getCommunicationData(obj.getUuid());

            // dao.deleteCommuDetails(obj.getUuid());

            for (CommunicationDetails tempObj : commuTemp) {
                int flag = 0; // flag=0 means new entry otherwise updation on
                              // old
                              // entries...
                for (CommunicationDetails tempOld : tempForOldValues) {
                    this.log.debug("hereeeee" + tempOld.getType().length() + "\t" + tempObj.getType().length());
                    if (tempOld.getType().equals(tempObj.getType())) {
                        flag = 1;
                        break;
                    }
                }
                if (flag == 1) {
                    output2 =
                            output2 + dao.updateCommuDetails(obj.getUuid(), tempObj.getDetails(), tempObj.getType(), obj.getUpdatingUuid());
                } else if (flag == 0) {
                    output2 = output2 + dao.updateNewCommuDetails(obj.getUuid(), obj.getUpdatingUuid(), tempObj);

                }

            }

            // tempObj.setDependentID(String.valueOf(System.nanoTime()));
            // family details updation............
            List<FamilyDetails> tempForFamOldValues = new ArrayList<FamilyDetails>();
            tempForFamOldValues = dao.getFamilyData(obj.getUuid());

            // dao.deleteFamDetails(obj.getUuid());
            int itr = 0;
            for (FamilyDetails tempObj : famTemp) {

                int flag = 0; // flag=0 means new entry otherwise updation on
                              // old
                // entries...
                for (FamilyDetails tempOld : tempForFamOldValues) {

                    if (tempOld.getDependentID().equals(tempObj.getDependentID())) {
                        flag = 1;
                        break;
                    }
                }

                if (flag == 1) {
                    output3 = output3 + dao.updateFamDetails(obj.getUuid(), obj.getUpdatingUuid(), tempObj);
                } else {
                    tempObj.setDependentID(String.valueOf(System.nanoTime()) + itr);
                    itr++;
                    output3 = output3 + dao.updateAddFamDetails(obj.getUuid(), obj.getUpdatingUuid(), tempObj);

                }

            }

            // Updation of Address details..............

            List<AddressDetails> tempForAddressOldValues = new ArrayList<AddressDetails>();
            tempForAddressOldValues = dao.getAddressData(obj.getUuid());

            // dao.deleteAddressDetails(obj.getUuid());
            for (AddressDetails tempObj : addressTemp) {

                int flag = 0; // flag=0 means new entry otherwise updation on
                              // old
                // entries...
                for (AddressDetails tempOld : tempForAddressOldValues) {
                    if (tempOld.getType().equals(tempObj.getType())) {
                        flag = 1;
                        break;
                    }
                }
                if (flag == 1) {
                    output4 = output4 + dao.updateAddressDetails(obj.getUuid(), obj.getUpdatingUuid(), tempObj);
                } else {
                    output4 = output4 + dao.updateNewAddressDetails(obj.getUuid(), obj.getUpdatingUuid(), tempObj);
                }

            }

            // if (flag != 0) {
            // String
            // qury="UPDATE employeePersonalDetails SET lastName=bain WHERE UUID=2";
            // output = dao.updateTableValues();
            // output = dao.updateTableValues(qury);
            // System.out.println(output + "raa");
            // toBeReturned = dao.getTableValues("11");
            // toBeReturned.employeeId = "sda";
            // toBeReturned.firstName = "rahul sharmaaa";
            // }

            ResponseStatus tempResponse = new ResponseStatus();
            // if ((output0 != 0) && (output4 != 0) && (output1 != 0) &&
            // (output2 != 0)) {
            // tempResponse.setCode(0);
            // tempResponse.setMessage("updated_successfully");
            //
            // } else {
            //
            // tempResponse.setCode(1);
            // tempResponse.setMessage("not_updated");
            //
            // }

            // toBeReturned.setStatus(tempResponse);
            h.commit();
            h.close();
            // System.out.println(toBeReturned.employeeId);
            return toBeReturned;

        } catch (RuntimeException e) {
            if (h != null) {
                h.rollback();
            }
            this.log.error("Error while updating AddOn. Exception: " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            if (h != null) {
                h.close();
            }

        }

    }
}
