/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.dao.LoginInternalDao;

/**
 * Entity manager for Login Entity.
 * @author chaitanya
 */
public class EmployeeExistsEntity {
    private static final Logger LOG = LoggerFactory.getLogger(LoginEntityManager.class);

    /**
     * @param emailId input from user
     * @return boolean if exists or not
     * @throws ClassNotFoundException notfound
     */
    public boolean checkUid(String emailId) throws ClassNotFoundException {
        LOG.debug("IN LOGIN INST");
        LoginInternalDao idao = null;
        try {
            idao = BaseEntityManager.getDbi().open(LoginInternalDao.class);
            String uuid = idao.fetchuuid(emailId);
            return (uuid != null);

        } catch (Exception e) {
            return false;
        }
    }
}
