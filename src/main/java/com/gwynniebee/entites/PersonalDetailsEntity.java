/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.restlet.responses.Details;
import com.gwynniebee.dao.EmpPerDetaildao;
import com.gwynniebee.rest.common.response.ResponseStatus;

//import com.gwynniebee.dao.logindao;

/**
 * @author rahul
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonalDetailsEntity {
    private static final Logger LOG = LoggerFactory.getLogger(PersonalDetailsEntity.class);

    // public static void main(String[] a) throws JsonParseException,
    // ClassNotFoundException, IOException {
    // PersonalDetailsEntity aaaa = new PersonalDetailsEntity();
    // aaaa.getPersonalInfo("1");
    //
    // }

    /**
     * @param uuid uuid of the user.
     * @return return details object which contain all the information of the
     *         user.
     * @throws ClassNotFoundException throws exception is any used class is
     *             missing.
     */
    public Details getPersonalInfo(String uuid) throws ClassNotFoundException {
        ResponseStatus repst = new ResponseStatus();
        Details toBeReturned = new Details();
        // try {
        EmpPerDetaildao dao = BaseEntityManager.getDbi().open(EmpPerDetaildao.class);
        LOG.debug("UUID:" + uuid);

        EmployeeDetails tempEmpDetail = new EmployeeDetails();
        tempEmpDetail.setPersonalDetails(dao.getPersonalData(uuid));
        LOG.debug("UUID:" + tempEmpDetail.getPersonalDetails());
        tempEmpDetail.setAddressDetails(dao.getAddressData(uuid));
        LOG.debug("UUID:" + tempEmpDetail.getAddressDetails());
        tempEmpDetail.setFamilyDetails(dao.getFamilyData(uuid));
        LOG.debug("UUID:" + tempEmpDetail.getFamilyDetails());
        tempEmpDetail.setCommunicationDetails(dao.getCommunicationData(uuid));
        LOG.debug("UUID:" + tempEmpDetail.getCommunicationDetails());

        toBeReturned.setLoginCred(dao.getLoginData(uuid));
        toBeReturned.setEmployeeDetails(tempEmpDetail);
        // repst.setCode(0);
        // repst.setMessage("Successful");
        // toBeReturned.setStatus(repst);
        // } catch (Exception e) {
        // repst.setCode(1);
        // repst.setMessage(e.getMessage());
        // toBeReturned.setStatus(repst);
        // }
        return toBeReturned;
    }
}
