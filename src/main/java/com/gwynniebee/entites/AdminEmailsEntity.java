/**
 * Copyright 2014 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import java.util.List;

import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.dao.AdminEmailsDao;

/**
 * @author harsh
 */
public class AdminEmailsEntity {

    private static final AdminEmailsEntity MANAGER_INSTANCE = new AdminEmailsEntity();
    private static final Logger LOG = LoggerFactory.getLogger(AdminEmailsEntity.class);

    /**
     * @return uuids
     */
    public List<String> getAdminuuidListcall() {

        List<String> uuidList = null;
        AdminEmailsDao dao = null;

        Handle h = null;
        try {
            h = BaseEntityManager.getDBIh();
        } catch (ClassNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        h.begin();
        dao = h.attach(AdminEmailsDao.class);
        try {

            uuidList = dao.getAdminuuidList();

        } catch (Exception e) {
            LOG.info("unable to retrieve uuid list of admins ");

        } finally {

            h.close();

        }
        return uuidList;

    }

    /**
     * @param uuid to be set
     * @return email
     */
    public String getAdminEmailcall(String uuid) {

        AdminEmailsDao dao = null;
        String adminEmail = null;
        Handle h = null;
        try {
            h = BaseEntityManager.getDBIh();
        } catch (ClassNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        h.begin();
        dao = h.attach(AdminEmailsDao.class);
        try {

            adminEmail = dao.getAdminEmailId(uuid);

        } catch (Exception e) {
            LOG.info("unable to get email_id of admin of uuid " + uuid);

        } finally {

            h.close();

        }
        return adminEmail;

    }

    /**
     * @return an instance
     */
    public static AdminEmailsEntity getInstance() {
        return MANAGER_INSTANCE;
    }
}
