/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

/**
 * Entity manager for BaseEntity.
 * @author skishore
 */
public final class BaseEntityManager {

    private BaseEntityManager() {

    }

    private static DBI dbi;

    /**
     * @return dbi
     */
    public static DBI getDbi() {
        return dbi;
    }

    /**
     * @param dbi sets DBI
     */
    public static void setDbi(DBI dbi) {
        BaseEntityManager.dbi = dbi;
    }

    /**
     * @param url driver
     * @param username username
     * @param password password
     * @param driver COM
     */
    public static void setDbi(String url, String username, String password, String driver) {
        try {
            Class.forName(driver);
            dbi = new DBI(url, username, password);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @return the db handle instance
     * @throws ClassNotFoundException is thrown
     */
    public static Handle getDBIh() throws ClassNotFoundException {
        Handle h;
        // DBI("jdbc:mysql://localhost/Employee_Management","root","root");
        h = dbi.open();
        return h;
    }
}
