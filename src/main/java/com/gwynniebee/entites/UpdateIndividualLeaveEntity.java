/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.IndividualLeaveObject;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.dao.LeavesDao;
import com.gwynniebee.rest.common.response.ResponseStatus;

//import com.gwynniebee.dao.logindao;
/**
 * @author rahul
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateIndividualLeaveEntity {

    /**
     * @param obj detailed information of the user.
     * @return returns the response of the query.
     * @throws ClassNotFoundException throws exception if any of the used class
     *             is missing.
     */
    public Generichandler updateLeave(IndividualLeaveObject obj) throws ClassNotFoundException {
        Handle h = null;
        Generichandler toBeReturned = new Generichandler();
        ResponseStatus repst = new ResponseStatus();
        try {
            h = BaseEntityManager.getDBIh();
            LeavesDao dao = null;
            h.begin();
            dao = h.attach(LeavesDao.class);
            ResponseStatus temp = new ResponseStatus();
            int output = 0;
            output = dao.updateLeaves(obj);
            h.commit();
            if (output == 0) {
                repst.setCode(2);
            } else {
                repst.setCode(ResponseStatus.CODE_SUCCESS);
            }
            repst.setMessage("Successful");
        } catch (Exception e) {
            if (h != null) {
                h.rollback();
            }
            repst.setCode(1);
            repst.setMessage(e.getMessage());
        } finally {
            h.close();
        }
        toBeReturned.setStatus(repst);
        return toBeReturned;
    }
}
