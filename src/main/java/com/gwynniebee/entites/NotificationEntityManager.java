/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import java.sql.Date;
import java.util.ArrayList;

import com.gwynniebee.backoffice.restlet.responses.NotificationHandler;
import com.gwynniebee.dao.Notificationdao;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * NOtification Entity Manager.
 * @author skishore
 */
public class NotificationEntityManager {

    /**
     * @return list of dates
     */
    public NotificationHandler getpendingattendance() {
        NotificationHandler nh = new NotificationHandler();
        ResponseStatus status = new ResponseStatus();
        try {
            Notificationdao dao = BaseEntityManager.getDbi().open(Notificationdao.class);
            ArrayList<Date> dt = (ArrayList<Date>) dao.getpendingattendancedates();
            nh.setAld(dt);
            status.setCode(ResponseStatus.CODE_SUCCESS);
            status.setMessage("Successful");

        } catch (Exception e) {
            status.setCode(1);
            status.setMessage(e.getMessage());
        } finally {
            nh.setStatus(status);
        }
        return nh;
    }

}
