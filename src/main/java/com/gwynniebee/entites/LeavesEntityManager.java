/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.entites;

import java.sql.Date;
import java.util.ArrayList;

import com.gwynniebee.backoffice.objects.LeavesDetails;
import com.gwynniebee.backoffice.restlet.responses.LeavesResponse;
import com.gwynniebee.dao.LeavesDao;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * Entity manager for Leaves Entity.
 * @author skishore
 */
public class LeavesEntityManager {
    /**
     * @param uuid of user
     * @param startdate of attendnace enquiry
     * @param enddate of attendance enquiry
     * @return result
     * @throws ClassNotFoundException exception
     */
    public LeavesResponse checkleaves(String uuid, Date startdate, Date enddate) throws ClassNotFoundException {
        LeavesResponse leaves = new LeavesResponse();
        ResponseStatus rs = new ResponseStatus();
        try {
            LeavesDao dao = BaseEntityManager.getDbi().open(LeavesDao.class);
            leaves.setLeavesdetails((ArrayList<LeavesDetails>) dao.queryleaves(uuid, startdate, enddate));
            leaves.setStatus(rs);
            rs.setCode(ResponseStatus.CODE_SUCCESS);
            rs.setMessage(ResponseStatus.MESSAGE_SUCCESS);
        } catch (Exception e) {
            rs.setCode(1);
            rs.setMessage(e.getMessage());
        }

        return leaves;
    }
}
