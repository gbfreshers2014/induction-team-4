/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.resources;

//import java.io.IOException;

//import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.codehaus.jackson.JsonParseException;
import org.restlet.data.Form;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.SearchResult;
import com.gwynniebee.backoffice.objects.SearchResultArray;
import com.gwynniebee.backoffice.restlet.responses.Details;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.backoffice.restlet.responses.SearchHandler;
import com.gwynniebee.entites.AddEmpEntity;
import com.gwynniebee.entites.SearchEntityManager;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * Employee Manageer.
 * @author rahul,shyam
 */
public class EmployeeManage extends AbstractServerResource {
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeManage.class);
    private String tempUuid = "";
    private final Logger log = org.slf4j.LoggerFactory.getLogger(EmployeeManage.class);

    // @Override
    // protected void doInit() {
    // super.doInit();
    // this.log.info("in_doInit");
    // this.tempUuid = (String) this.getRequestAttributes().get("UUID");
    // // this.tempUuid = "11";
    //
    // // System.out.println(this.tempUuid);
    // if (Strings.isNullOrEmpty(this.tempUuid)) {
    // this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
    // }
    // }

    // public static void main(String[] a) throws JsonParseException,
    // ClassNotFoundException, IOException {
    // EmployeePersonalDetails aaa = new EmployeePersonalDetails();
    // aaa.getEmployee();
    //
    // }

    // private static final Logger LOG =
    // LoggerFactory.getLogger(loginASR.class);

    // loginObject handles mail and password
    /**
     * @param obj takes the object of details class.
     * @return returns the response of the query.
     * @throws JsonParseException throws error if present in parsing details
     *             object.
     * @throws ClassNotFoundException throws error if any class in not found
     *             while compiling.
     */
    @Post
    public Generichandler addEmp(Details obj) throws JsonParseException, ClassNotFoundException {

        Generichandler response = new Generichandler();

        // Get UUID here...............................
        // this.tempUuid = "12";
        // String uuid = this.tempUuid;

        // Process uuid from here.................
        // System.out.println(this.tempUuid + "dsad");
        // System.out.println(this.tempUuid);

        AddEmpEntity temp = new AddEmpEntity();

        try {
            response = temp.addEmployee(obj);
        } catch (Exception e) {
            ResponseStatus tempResp = new ResponseStatus();
            tempResp.setMessage(e.getMessage());
            tempResp.setCode(1);
            response.setStatus(tempResp);

        }
        //
        // System.out.println(response.firstName + response.UUID);
        //
        // // response.firstName = "rahul";
        // // response.UUID = this.tempUuid;
        return response;

    }

    /**
     * @return returns the uuid from url.
     */
    public String getTempUuid() {
        return this.tempUuid;
    }

    /**
     * @param tempUuid sets the uuid.
     */
    public void setTempUuid(String tempUuid) {
        this.tempUuid = tempUuid;
    }

    /**
     * @return details
     * @throws ClassNotFoundException exception
     */
    @Get
    public SearchHandler searchEmployee() throws ClassNotFoundException {

        ArrayList<String> parameters = new ArrayList<String>();
        parameters = this.getPatameters();
        HashMap<String, Integer> hm = new HashMap<String, Integer>();
        SearchHandler response = new SearchHandler();
        SearchHandler response1 = new SearchHandler();
        int index = 0;
        SearchEntityManager sem = new SearchEntityManager();
        for (String currentQueryParam : parameters) {
            LOG.debug("CURRENT:" + currentQueryParam);
            response = sem.searchQuery(currentQueryParam);
            SearchResultArray sra = new SearchResultArray();
            ArrayList<SearchResult> temp = response.getList();
            sra.setEmployeeList(temp);
            LOG.debug("CURRENT:" + currentQueryParam + sra.getEmployeeList().size());
            LOG.debug(response.getStatus().getMessage() + response.getList().size());
            // response.getList().clear();
            if (response.getStatus().getCode() == 0) {
                LOG.debug("search query worked");
                ArrayList<SearchResult> al = sra.getEmployeeList();
                Iterator<SearchResult> it = al.iterator();
                LOG.debug(response.getStatus().getMessage() + response.getList().size());
                while (it.hasNext()) {
                    SearchResult sr = it.next();
                    LOG.debug("new employee " + sr.getPersonalDetails().getUuid());
                    if (!hm.containsKey(sr.getPersonalDetails().getUuid())) {
                        hm.put(sr.getPersonalDetails().getUuid(), index++);
                        response1.getList().add(sr);
                    } else {
                        int val = response1.getList().get(hm.get(sr.getPersonalDetails().getUuid())).getValue();
                        response1.getList().get(hm.get(sr.getPersonalDetails().getUuid())).setValue(val++);
                    }
                }
            }
        }
        ResponseStatus rs = new ResponseStatus();
        rs.setCode(0);

        if ((response1.getList() == null) || response1.getList().isEmpty()) {
            rs.setMessage("NOT FOUND");
        } else {
            rs.setMessage("FOUND");
        }
        response1.setStatus(rs);
        return response1;
    }

    /**
     * @return
     */
    ArrayList<String> getPatameters() {
        ArrayList<String> queryParams = new ArrayList<String>();
        Form response = this.getRequest().getResourceRef().getQueryAsForm();
        String retrievedConcatenated = response.getValues("param");
        LOG.debug(retrievedConcatenated);
        for (String it : retrievedConcatenated.split(" ")) {
            queryParams.add(it);
        }
        return queryParams;
    }

}
