/**
 * Copyright 2014 GwynnieBee Inc.
 */

package com.gwynniebee.backoffice.restlet.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.restlet.resource.Get;
import org.slf4j.Logger;

import com.gwynniebee.backoffice.restlet.responses.AdminEmailsResponseClass;
import com.gwynniebee.entites.AdminEmailsEntity;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

//import com.gwynniebee.rest.service.restlet.resources.;
/**
 * @author harsh
 */
public class AdminEmailsForTeam1 extends AbstractServerResource {

    static final Logger LOG = org.slf4j.LoggerFactory.getLogger(AdminEmailsForTeam1.class);

    /**
     * @return response
     * @throws IOException IOexception
     * @throws ClassNotFoundException classnotfound
     */
    @Get
    public AdminEmailsResponseClass getAdminEmails() throws IOException, ClassNotFoundException {

        AdminEmailsResponseClass response = new AdminEmailsResponseClass();

        List<String> uuids = AdminEmailsEntity.getInstance().getAdminuuidListcall();

        List<String> emails = new ArrayList<String>();
        for (int i = 0; i < uuids.size(); i++) {
            LOG.info(uuids.get(i));
            emails.add(AdminEmailsEntity.getInstance().getAdminEmailcall(uuids.get(i)));
            LOG.info(emails.get(i));

        }
        response.setEmails(emails);
        ResponseStatus status = new ResponseStatus();
        status.setCode(0);
        status.setMessage("list of admin emails are being sent to inventory management team1");

        response.setStatus(status);

        return response;

    }

}
