/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.resources;

import java.io.IOException;

import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.LoginObject;
import com.gwynniebee.backoffice.restlet.responses.LoginHandler;
import com.gwynniebee.entites.LoginEntityManager;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * Login Server Resource.
 * @author skishore
 */
public class LoginASR extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(LoginASR.class);

    /**
     * @param logininst is the input from user
     * @return response
     * @throws IOException io error
     * @throws ClassNotFoundException error
     */
    @Post
    public LoginHandler handlelogin(LoginObject logininst) throws IOException, ClassNotFoundException {
        LOG.info("request " + logininst.toString());
        LoginEntityManager loginentityinstance = new LoginEntityManager();
        LoginHandler loginhandlerinstance = loginentityinstance.authenticate(logininst);
        if (loginhandlerinstance.getRole().length() == 0 || loginhandlerinstance.getUuid().length() == 0) {
            loginhandlerinstance.getStatus().setMessage("Wrong Authentication");
        }
        return loginhandlerinstance;
    }
}
