/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.resources;

import java.sql.SQLException;

import liquibase.exception.LiquibaseException;

import org.restlet.resource.Get;

import com.gwynniebee.backoffice.restlet.responses.LiquibaseHandler;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * Liquibase Server Resource.
 * @author skishore
 */
public class LiquibaseASR extends AbstractServerResource {

    static final int A = 200;

    /**
     * @return nothing
     * @throws SQLException error processing
     * @throws LiquibaseException exceptioni
     */
    @Get
    public LiquibaseHandler synchronizeDB() throws SQLException, LiquibaseException {
        ResponseStatus rs = new ResponseStatus();
        LiquibaseHandler lh = new LiquibaseHandler();
        lh.synchronizeDB();
        rs.setCode(A);
        rs.setMessage("SuccessFul");
        lh.setStatus(rs);
        return null;
    }
}
