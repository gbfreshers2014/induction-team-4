/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.resources;

import org.restlet.resource.Get;

import com.gwynniebee.backoffice.restlet.responses.NotificationHandler;
import com.gwynniebee.entites.NotificationEntityManager;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * Notificication Handler.
 * @author skishore
 */
public class GenerateNotificationASR extends AbstractServerResource {

    /**
     * @return to be updated attendance
     */
    @Get
    public NotificationHandler generatenotification() {
        NotificationEntityManager ner = new NotificationEntityManager();
        NotificationHandler nh = ner.getpendingattendance();
        return nh;
    }
}
