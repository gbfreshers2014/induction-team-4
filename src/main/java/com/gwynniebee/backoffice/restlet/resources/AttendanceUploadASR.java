/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.resources;

/**
 * Attendance Server Resource.
 * @author ASK
 */
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.Constants;
import com.gwynniebee.backoffice.objects.DBConstants;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.cloudio.CloudAccessClient;
import com.gwynniebee.cloudio.CloudAccessClientFactory;
import com.gwynniebee.cloudio.Entry;
import com.gwynniebee.cloudio.SaveOptions;
import com.gwynniebee.entites.TaskEntityManager;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * Uploads Attendance.
 * @author skishore
 */
public class AttendanceUploadASR extends AbstractServerResource {
    private final int threshold = 1000240;
    private byte[] uploadBytes;
    public static final String CONST_UTF_8 = "UTF-8";
    private static final Logger LOG = LoggerFactory.getLogger(LoginASR.class);

    /**
     * @param entity is file action pair
     * @return response
     * @throws Exception is thrown
     */
    @Post("multipart")
    public Generichandler uploadAttendance(Representation entity) throws Exception {
        Generichandler gh = new Generichandler();
        LOG.debug("HERE AT ATTENDNACE UPLOAD");
        ResponseStatus repst = getRespStatus();
        LOG.debug("HERE AT FILE LOAD");
        try {
            Map<String, String> rv = fileuploader();
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            sdf1.setTimeZone(TimeZone.getTimeZone("India"));
            java.util.Date tempDate = sdf1.parse(rv.get("date"));
            java.sql.Date date = new java.sql.Date(tempDate.getTime());
            org.joda.time.DateTime dt = new org.joda.time.DateTime(date);

            LOG.debug("HERE RV" + rv.get("action"));
            int code;
            String uploadinguuid = rv.get("uploadingUuid");
            if (rv.get("action").equals("uploaded")) {
                LOG.debug("HERE AT UPLOADc" + dt);
                CloudAccessClient s3client = CloudAccessClientFactory.getS3CloudAccessClient(DBConstants.getBucket());
                Entry entry = s3client.createNewEntry(Constants.ENTRYNAME, 0, dt);
                SaveOptions saveOption = new SaveOptions().withPublicAccess(true);
                LOG.debug("HERE AT savingUPLOAD");
                s3client.save(entry, Constants.FILENAME, rv.get("attendance"), saveOption);
                s3client.updateLatest(entry, null);
            }
            LOG.debug("HERE AT WILL DO UPLOAD");
            TaskEntityManager tem = new TaskEntityManager();
            repst = tem.updatetasks(date, rv.get("action"), uploadinguuid);

        } catch (Exception e) {
            repst.setCode(1);
            repst.setMessage(e.getMessage());
        }
        gh.setStatus(repst);
        return gh;
    }

    /**
     * @return file contnent in bytes
     */
    public byte[] getUploadBytes() {
        return uploadBytes;
    }

    /**
     * @param uploadBytes in bytes
     */
    public void setUploadBytes(byte[] uploadBytes) {
        this.uploadBytes = uploadBytes;
    }

    /**
     * @return the parameters of input
     * @throws Exception
     */
    Map<String, String> fileuploader() throws Exception {
        Map<String, String> rv = new HashMap<String, String>();
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(threshold);
        RestletFileUpload upload = new RestletFileUpload(factory);
        List<FileItem> items = upload.parseRepresentation(getRequestEntity());
        // items = upload.parseRequest(this.getRequest());
        uploadBytes = null;
        if (items != null) {
            for (FileItem item : items) {
                String fiFieldName = item.getFieldName();

                if (item.getFieldName().equals("file")) {
                    uploadBytes = item.get();
                    rv.put("attendance", new String(uploadBytes));
                    LOG.debug(new String(uploadBytes));
                } else {
                    String getValue = new String(item.get(), CONST_UTF_8);
                    rv.put(fiFieldName, getValue);
                }
            }
        } else {
            StringBuilder sbMessage = new StringBuilder();
            sbMessage.append("Invalid Media Type: ");
            sbMessage.append(this.getRequestEntity().getMediaType());
            throw new Exception(sbMessage.toString());
        }

        return rv;
    }

    /**
     * @return string
     */
    String stringbuilded() {

        String str = new String(uploadBytes);
        LOG.debug(str);
        return str;
        /*
         * StringBuilder strbuild=new StringBuilder(); for(byte b : ) { Byte
         * b_b=b; b_b.toString(); LOG.debug(String.valueOf(b));
         * LOG.debug("-"+String.valueOf(b_b));
         * strbuild.append(String.valueOf(b)); } return strbuild.toString();
         */
    }
}
