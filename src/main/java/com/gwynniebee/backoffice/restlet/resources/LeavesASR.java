/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.resources;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.restlet.data.Form;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

import com.gwynniebee.backoffice.objects.IndividualLeaveObject;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.backoffice.restlet.responses.LeavesResponse;
import com.gwynniebee.entites.LeavesEntityManager;
import com.gwynniebee.entites.UpdateIndividualLeaveEntity;
import com.gwynniebee.rest.common.response.AbstractResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * Leaves Server Resource.
 * @author skishore
 */
public class LeavesASR extends AbstractServerResource {
    /**
     * @return respoonse
     * @throws ParseException error
     * @throws ClassNotFoundException error
     */
    @Get
    public LeavesResponse employeeSearch() throws ParseException, ClassNotFoundException {
        ResponseStatus resp = this.getRespStatus();
        String uuid = (String) this.getRequest().getAttributes().get("uuid");
        Form fm = this.getRequest().getResourceRef().getQueryAsForm();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date startDate = sdf1.parse(fm.getFirstValue("start_date"));
        java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
        java.util.Date endDate = sdf1.parse(fm.getFirstValue("end_date"));
        java.sql.Date sqlendDate = new java.sql.Date(endDate.getTime());
        LeavesEntityManager lem = new LeavesEntityManager();
        LeavesResponse leavesresp = lem.checkleaves(uuid, sqlStartDate, sqlendDate);
        if (leavesresp.getLeavesdetails() == null) {
            leavesresp.setStatus(this.getRespStatus());
        }
        return leavesresp;
    }

    /**
     * @param obj object of Individual leave object
     * @return returns the response of the request.
     * @throws IOException throws exception
     * @throws ClassNotFoundException throws exception when any of the class is
     *             not found.
     */
    @Post
    public AbstractResponse updateIndividualAttendance(IndividualLeaveObject obj) throws IOException, ClassNotFoundException {
        // ResponseStatus status = new ResponseStatus();
        // DeleteEntityManager deleteentityinstance = new DeleteEntityManager();
        // DeleteObject deleteinst = new DeleteObject();
        // deleteinst.setUUID((String)
        // this.getRequest().getAttributes().get("UUID"));

        // to get params in url
        // deleteinst.setUpdatinguuid(this.getRequest().getResourceRef().getQueryAsForm().getValues("updatingUuid"));

        String uuid = (String) this.getRequest().getAttributes().get("uuid");
        obj.setUuid(uuid);

        // String updatingUuid =
        // this.getRequest().getResourceRef().getQueryAsForm().getValues("updatingUuid");
        // String date =
        // this.getRequest().getResourceRef().getQueryAsForm().getValues("date");

        // int lro = deleteentityinstance.changeemployemntstatus(deleteinst);
        // status.setCode(lro);
        // status.setMessage("MESSAGE_SUCCESS");
        Generichandler response = new Generichandler();
        UpdateIndividualLeaveEntity temp = new UpdateIndividualLeaveEntity();
        ResponseStatus repst = new ResponseStatus();
        try {
            response = temp.updateLeave(obj);
        } catch (Exception e) {
            repst.setCode(1);
            repst.setMessage(e.getMessage());
            response.setStatus(repst);
        }

        // response.setStatus(status);
        return response;
    }

}
