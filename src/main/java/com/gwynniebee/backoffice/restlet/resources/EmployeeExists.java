/**
 * Copyright 2014 GwynnieBee Inc.
 */

package com.gwynniebee.backoffice.restlet.resources;

import org.codehaus.jackson.JsonParseException;
import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.slf4j.Logger;

import com.gwynniebee.backoffice.restlet.responses.DummyResponse;
import com.gwynniebee.entites.EmployeeExistsEntity;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * @author chaitanya
 */
public class EmployeeExists extends AbstractServerResource {
    /**
     * @return returns the uuid.
     */
    public String getTempUuid() {
        return this.tempUuid;
    }

    /**
     * @param tempUuid sets the uuid.
     */
    public void setTempUuid(String tempUuid) {
        this.tempUuid = tempUuid;
    }

    private String tempUuid = "";
    private final Logger log = org.slf4j.LoggerFactory.getLogger(EmployeeExists.class);

    /**
     * @return returns the json object which contains all the information of the
     *         user.
     * @throws JsonParseException parses the object and throws errors if
     *             present.
     * @throws ClassNotFoundException throws error if any of the class is
     *             missing.
     */
    @Get
    public DummyResponse getEmployee() throws JsonParseException, ClassNotFoundException {
        this.log.debug("Nobody is here");
        boolean exists;
        this.tempUuid = (String) this.getRequest().getAttributes().get("UUID");
        EmployeeExistsEntity eexists = new EmployeeExistsEntity();
        exists = eexists.checkUid(this.tempUuid);
        DummyResponse dR = new DummyResponse();
        if (exists) {
            this.getRespStatus().setCode(ResponseStatus.CODE_SUCCESS);
            this.getRespStatus().setMessage("Uuid exists");
            this.setStatus(Status.SUCCESS_ACCEPTED);
        } else {
            this.getRespStatus().setCode(ResponseStatus.ERR_CODE_RESOURCE_NOT_FOUND);
            this.getRespStatus().setMessage("Not found");
            this.setStatus(Status.SUCCESS_ACCEPTED);
        }
        dR.setStatus(getRespStatus());

        return dR;
    }
}
