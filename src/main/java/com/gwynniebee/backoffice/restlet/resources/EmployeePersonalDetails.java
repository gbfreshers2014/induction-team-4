/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.resources;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.restlet.data.Status;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.slf4j.Logger;

import com.gwynniebee.backoffice.objects.DeleteObject;
import com.gwynniebee.backoffice.restlet.responses.Details;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.backoffice.restlet.responses.SearchHandler;
import com.gwynniebee.entites.DeleteEntityManager;
import com.gwynniebee.entites.PersonalDetailsEntity;
import com.gwynniebee.entites.SearchEntityManager;
import com.gwynniebee.entites.UpdateDetailsEntity;
import com.gwynniebee.rest.common.response.AbstractResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * @author rahul
 */
// import com.gwynniebee.rest.service.restlet.resources.;
public class EmployeePersonalDetails extends AbstractServerResource {

    /**
     * @return returns the uuid.
     */
    public String getTempUuid() {
        return this.tempUuid;
    }

    /**
     * @param tempUuid sets the uuid.
     */
    public void setTempUuid(String tempUuid) {
        this.tempUuid = tempUuid;
    }

    private String tempUuid = "";
    private final Logger log = org.slf4j.LoggerFactory.getLogger(EmployeePersonalDetails.class);

    /**
     * @return returns the json object which contains all the information of the
     *         user.
     * @throws JsonParseException parses the object and throws errors if
     *             present.
     * @throws ClassNotFoundException throws error if any of the class is
     *             missing.
     */
    @Get
    public Details getEmployee() throws JsonParseException, ClassNotFoundException {
        this.log.debug("rahul is here");
        Details response = new Details();
        this.tempUuid = (String) this.getRequest().getAttributes().get("UUID");
        PersonalDetailsEntity temp = new PersonalDetailsEntity();
        // response = temp.getPersonalInfo(this.tempUuid);

        ResponseStatus repst = new ResponseStatus();
        try {
            response = temp.getPersonalInfo(this.tempUuid);
            if (response.getEmployeeDetails().getPersonalDetails() == null) {
                this.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
                // this.log.debug("here.................................");
                repst.setCode(1);
                repst.setMessage("UrlNotFound");
            } else {
                repst.setCode(0);
                repst.setMessage("Successful");
            }
            response.setStatus(repst);

        } catch (Exception e) {
            repst.setCode(1);
            repst.setMessage(e.getMessage());
            response.setStatus(repst);
        }

        return response;
    }

    /**
     * @param obj takes the object of details class.
     * @return return the status of the query.
     * @throws JsonParseException parses the object and throws errors if
     *             present.
     * @throws ClassNotFoundException throws error if any of the class is
     *             missing.
     */
    @Put
    // public StatusResponseObject updateEmployee(Details obj) throws
    // JsonParseException, IOException, ClassNotFoundException
    public Generichandler updateEmployee(Details obj) throws JsonParseException, ClassNotFoundException {
        this.tempUuid = (String) this.getRequest().getAttributes().get("UUID");
        Generichandler response = new Generichandler();
        obj.setUuid(this.tempUuid);
        UpdateDetailsEntity temp = new UpdateDetailsEntity();

        ResponseStatus repst = new ResponseStatus();
        try {
            response = temp.updateDetails(obj);
            repst.setCode(0);
            repst.setMessage("Successful");
            response.setStatus(repst);

        } catch (Exception e) {
            repst.setCode(1);
            repst.setMessage(e.getMessage());
            if (e.getMessage().contains("com.mysql.jdbc")) {
                this.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
            }
            response.setStatus(repst);
        }

        return response;
        // return obj;
    }

    //
    // @Post
    // public PersonalDetails a() {
    // }
    /**
     * @return response
     * @throws IOException io error
     * @throws ClassNotFoundException class not found
     */
    @Delete
    public AbstractResponse handledelete() throws IOException, ClassNotFoundException {
        ResponseStatus status = new ResponseStatus();
        DeleteEntityManager deleteentityinstance = new DeleteEntityManager();
        DeleteObject deleteinst = new DeleteObject();
        deleteinst.setUUID((String) this.getRequest().getAttributes().get("UUID"));

        // to get params in url
        deleteinst.setUpdatinguuid(this.getRequest().getResourceRef().getQueryAsForm().getValues("updatingUuid"));
        status = deleteentityinstance.changeemployemntstatus(deleteinst);
        Generichandler response = new Generichandler();
        response.setStatus(status);
        return response;
    }

    /**
     * @throws ClassNotFoundException class not found
     * @return all employee details
     */
    @Post
    public SearchHandler getallemployees() throws ClassNotFoundException {
        SearchHandler sh = new SearchHandler();
        SearchEntityManager sem = new SearchEntityManager();
        sh = (sem.retieveall());
        return sh;
    }
}
