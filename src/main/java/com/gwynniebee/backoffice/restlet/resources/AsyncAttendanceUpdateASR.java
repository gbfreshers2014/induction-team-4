/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.resources;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.restlet.resource.Put;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csvreader.CsvReader;
import com.gwynniebee.backoffice.objects.Constants;
import com.gwynniebee.backoffice.objects.DBConstants;
import com.gwynniebee.backoffice.objects.TaskObject;
import com.gwynniebee.backoffice.objects.Taskdetails;
import com.gwynniebee.backoffice.restlet.responses.Generichandler;
import com.gwynniebee.cloudio.CloudAccessClient;
import com.gwynniebee.cloudio.CloudAccessClientFactory;
import com.gwynniebee.cloudio.Entry;
import com.gwynniebee.entites.AttendanceEntityManager;
import com.gwynniebee.entites.TaskEntityManager;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

/**
 * ASync job to update attendance.
 * @author skishore
 */
public class AsyncAttendanceUpdateASR extends AbstractServerResource {
    public static final String CONST_UTF_8 = "UTF-8";
    private static final Logger LOG = LoggerFactory.getLogger(AsyncAttendanceUpdateASR.class);
    private static final int MAX = 365;

    /**
     * @return status
     * @throws Exception general
     */
    @Put
    public Generichandler attendnaceUpdate() throws Exception {
        int code = 0;
        ResponseStatus status = new ResponseStatus();
        Generichandler ge = new Generichandler();
        try {
            TaskEntityManager tem = new TaskEntityManager();
            TaskObject tobj = tem.gettasks();

            if (tobj != null) {
                LOG.debug("INTO ASR" + tobj.getTask().size());
                for (Taskdetails tde : tobj.getTask()) {
                    LOG.debug("INTO LOOP");
                    if (tde.getAction().equals("uploaded")) {
                        LOG.debug("FILE ACQUIRING");
                        CloudAccessClient s3client = CloudAccessClientFactory.getS3CloudAccessClient(DBConstants.getBucket());
                        List<Entry> latest = s3client.getRecentEntries(Constants.ENTRYNAME, 0, MAX);
                        // LOG.debug("entrie" + entries.size());
                        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd");
                        // org.joda.time.DateTime dt = new
                        // org.joda.time.DateTime(tde.getDate());
                        Entry requiredentry = null;
                        for (Entry entry : latest) {
                            LOG.debug("entry.getUri():" + tde.getDate().toString());
                            LOG.debug("entry.getUri():" + fmt.format(tde.getDate()));
                            if (entry.getUri().contains(fmt.format(tde.getDate()))) {
                                requiredentry = entry;
                                break;
                            }
                        }
                        LOG.debug("ABOUT TO READ FILE");
                        if (requiredentry != null) {
                            LOG.debug(requiredentry.getUri());
                            LOG.debug("attendance" + tde.getDate() + ".csv");
                            byte[] data =
                                    s3client.readAs(requiredentry, com.gwynniebee.backoffice.objects.Constants.FILENAME, byte[].class);
                            String content = new String(data, Charset.forName(CONST_UTF_8));
                            LOG.debug("FILE ACK" + content);
                            HashSet<String> absent = new HashSet<String>();
                            absent = generateabsentlist(data);
                            AttendanceEntityManager aer = new AttendanceEntityManager();
                            ResponseStatus temp = aer.updateAttendance(absent, tde.getDate(), tde.getCreatedby());
                            status.setMessage(status.getMessage() + " " + temp.getMessage());
                            status.setCode(status.getCode() + temp.getCode());
                        }
                    } else {
                        AttendanceEntityManager aer = new AttendanceEntityManager();
                        ResponseStatus temp = aer.updateAttendance(tde.getDate(), tde.getAction(), tde.getCreatedby());
                        status.setMessage(status.getMessage() + " " + temp.getMessage());
                        status.setCode(status.getCode() + temp.getCode());
                    }
                    if (status.getCode() == 0) {
                        ResponseStatus temp = tem.deletetask(tde.getDate());
                        status.setMessage(status.getMessage() + " " + temp.getMessage());
                        status.setCode(status.getCode() + temp.getCode());
                    }
                }
            }
            java.util.Date currdate = new Date();
            java.sql.Date date = new java.sql.Date(currdate.getTime());
            ResponseStatus repst = tem.dailyinsert(date);
        } catch (Exception e) {
            status.setCode(1);
            status.setMessage(e.getMessage());
        }

        ge.setStatus(status);

        return ge;
    }

    /**
     * @param data data
     * @return absent list
     */
    private HashSet<String> generateabsentlist(byte[] data) {
        HashSet<String> absent = new HashSet<String>();
        CsvReader reader = null;
        try {
            InputStream stream = new ByteArrayInputStream(data);
            Reader streamReader = new InputStreamReader(stream);

            reader = new CsvReader(streamReader);
            reader.readHeaders();
            reader.getHeaders();
            while (reader.readRecord()) {
                try {
                    String emaild = reader.get("Employee");
                    String status = reader.get("Status");
                    // for future use
                    String date = reader.get("Date");
                    // for future use
                    absent.add(emaild);

                } catch (Throwable exception) {
                    LOG.error("Read CSV Error for content: " + exception.getMessage());
                }
            }
        } catch (Exception e) {
            LOG.error("Read CSV Error for bucket: " + e.getMessage());
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return absent;

    }
}
