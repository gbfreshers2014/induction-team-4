/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.responses;

//package com.gwynniebee.backoffice.objects;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.LoginCred;
import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * @author rahul
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
// import com.gwynniebee.rest.service.restlet.resources.*;
public class Details extends AbstractResponse {

    private String updatingUuid = "";
    private LoginCred loginCred = new LoginCred();
    private String uuid = "";
    private EmployeeDetails employeeDetails = new EmployeeDetails();

    /**
     * @return returns the status of the query.
     */

    /**
     * @param status sets the status.
     */

    /**
     * @return returns all details related to employee.
     */
    public EmployeeDetails getEmployeeDetails() {
        return this.employeeDetails;
    }

    /**
     * @param employeeDetails sets the employee all details.
     */
    public void setEmployeeDetails(EmployeeDetails employeeDetails) {
        this.employeeDetails = employeeDetails;
    }

    /**
     * @return returns the uuid of the user.
     */
    public String getUuid() {
        return this.uuid;
    }

    /**
     * @param uuid sets the uuid of the employee.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return login credentials.
     */
    public LoginCred getLoginCred() {
        return this.loginCred;
    }

    /**
     * @param loginCred login credentials of the user.
     */
    public void setLoginCred(LoginCred loginCred) {
        this.loginCred = loginCred;
    }

    /**
     * @return returns uuid of the admin who is updating user's profile
     */
    public String getUpdatingUuid() {
        return this.updatingUuid;
    }

    /**
     * @param updatingUuid uuid of the admin who is updating user's profile
     */
    public void setUpdatingUuid(String updatingUuid) {
        this.updatingUuid = updatingUuid;
    }

    // public String PRIMARY KEY (UUID)

}
