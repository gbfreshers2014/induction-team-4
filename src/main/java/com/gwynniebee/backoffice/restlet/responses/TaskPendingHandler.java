/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.responses;

import com.gwynniebee.backoffice.objects.TaskObject;
import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * The task notification system.
 * @author skishore
 */
public class TaskPendingHandler extends AbstractResponse {
    private TaskObject tobj;

    /**
     * @return taskobject
     */
    public TaskObject getTobj() {
        return tobj;
    }

    /**
     * @param tobj is set
     */
    public void setTobj(TaskObject tobj) {
        this.tobj = tobj;
    }

}
