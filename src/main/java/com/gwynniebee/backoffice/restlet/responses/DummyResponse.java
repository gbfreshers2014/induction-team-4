/**
 * Copyright 2014 GwynnieBee Inc.
 */

package com.gwynniebee.backoffice.restlet.responses;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * @author chaitanya
 */
public class DummyResponse extends AbstractResponse {

}
