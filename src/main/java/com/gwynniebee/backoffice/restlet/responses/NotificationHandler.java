/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.responses;

import java.sql.Date;
import java.util.ArrayList;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * Response class for notification system.
 * @author skishore
 */
public class NotificationHandler extends AbstractResponse {
    /**
     * @return is returned
     */
    public ArrayList<Date> getAld() {
        return ald;
    }

    /**
     * @param ald is set
     */
    public void setAld(ArrayList<Date> ald) {
        this.ald = ald;
    }

    private ArrayList<Date> ald;

}
