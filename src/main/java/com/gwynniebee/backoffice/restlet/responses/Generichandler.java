/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.responses;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * Generic rsponse for update/delete/insert.
 * @author skishore
 */
public class Generichandler extends AbstractResponse {
    private String uuid = "";

    /**
     * @return return the uuid of the user.
     */
    public String getUuid() {
        return this.uuid;
    }

    /**
     * @param uuid sets the uuid of the user.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
