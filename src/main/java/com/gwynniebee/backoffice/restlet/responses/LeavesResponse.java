/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.responses;

import java.util.ArrayList;

import com.gwynniebee.backoffice.objects.LeavesDetails;
import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * Leaves Response Object.
 * @author skishore
 */
public class LeavesResponse extends AbstractResponse {
    /**
     * @return leaves details
     */
    public ArrayList<LeavesDetails> getLeavesdetails() {
        return leavesdetails;
    }

    /**
     * @param leavesdetails is set
     */
    public void setLeavesdetails(ArrayList<LeavesDetails> leavesdetails) {
        this.leavesdetails = leavesdetails;
    }

    /**
     * @return count of leaves
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count is set
     */
    public void setCount(int count) {
        this.count = count;
    }

    private ArrayList<LeavesDetails> leavesdetails = new ArrayList<LeavesDetails>();
    private int count;
}
