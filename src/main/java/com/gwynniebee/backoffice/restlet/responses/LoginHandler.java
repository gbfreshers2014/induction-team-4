/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.responses;


import com.gwynniebee.rest.common.response.AbstractResponse;
/**
 * Search Response Object.
 * @author skishore
 */
public class LoginHandler extends AbstractResponse {
    /**
     * @return role of user
     */
    public String getRole() {
        return role;
    }
    /**
     * @param role is set
     */
    public void setRole(String role) {
        this.role = role;
    }
    /**
     * @return uuid
     */
    public String getUuid() {
        return uuid;
    }
    /**
     * @param uuid is set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    private String role = "";
    private String uuid = "";
}
