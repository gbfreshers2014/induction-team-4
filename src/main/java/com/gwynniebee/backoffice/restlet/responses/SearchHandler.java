/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.responses;

import java.util.ArrayList;

import com.gwynniebee.backoffice.objects.SearchResult;
import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * Search Response Object.
 * @author skishore
 */
public class SearchHandler extends AbstractResponse {

    /**
     * @return employeelist is returned
     */
    public ArrayList<SearchResult> getList() {
        return employeelist;
    }

    /**
     * @param list is set
     */
    public void setList(ArrayList<SearchResult> list) {
        this.employeelist = list;
    }

    private ArrayList<SearchResult> employeelist = new ArrayList<SearchResult>();

}
