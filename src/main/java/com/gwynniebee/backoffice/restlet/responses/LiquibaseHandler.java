/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.responses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.FileSystemResourceAccessor;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * @author skishore
 */
public class LiquibaseHandler extends AbstractResponse {
    /**
     * @throws SQLException sql error
     * @throws LiquibaseException liquibase error
     */
    public void synchronizeDB() throws SQLException, LiquibaseException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection con = DriverManager.getConnection("jdbc:mysql://192.168.1.100/Employee_Management", "root", "root");
        Liquibase liquibase = null;
        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(con));
        liquibase = new Liquibase("/log.xml", new FileSystemResourceAccessor(), database);
        liquibase.update(null);
    }
}
