/**
 * Copyright 2014 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.restlet.responses;

//package com.gwynniebee.backoffice.objects;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * @author harsh
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
// import com.gwynniebee.rest.service.restlet.resources.*;
public class AdminEmailsResponseClass extends AbstractResponse {

    private List<String> emails;

    /**
     * @return emails
     */
    public List<String> getEmails() {
        return emails;
    }

    /**
     * @param emails to be set
     */
    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

}
