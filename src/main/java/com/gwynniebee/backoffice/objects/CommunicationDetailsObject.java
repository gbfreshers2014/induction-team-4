/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * Wrapper Class for Employee Communication Details.
 * @author skishore
 */
public class CommunicationDetailsObject {
    /**
     * @return type of communication
     */
    public String getType() {
        return type;
    }

    /**
     * @param type sets it
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the value of type
     */
    public String getDetails() {
        return details;
    }

    /**
     * @param details sets the detail of type
     */
    public void setDetails(String details) {
        this.details = details;
    }

    private String details;
    private String type;

}
