/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * @author rahul sharma
 */
public class PersonalDetails {

    private String createdOn = "";
    private String createdBy = "";
    private String lastUpdateBy = "";
    private String employeeId = "";
    private String email = "";
    private String firstName = "";
    private String lastName = "";
    private String dob = "";
    private String doj = "";
    private String bloodGroup = "";
    private String employmentStatus = ""; // ENUM('employeed','retired'),
    private String designation = "";
    private String lastUpdatedOn = "";

    /**
     * @return returns the employee id.
     */
    public String getEmployeeId() {
        return this.employeeId;
    }

    /**
     * @param employeeId sets the employee id.
     */
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return return the employee email id.
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * @param email take employee email id as input.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return return first name of the employee.
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * @param firstName sets the first name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return return last name of the employee.
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * @param lastName takes last name of the employee as input.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return return date of birth of the employee.
     */
    public String getDob() {
        return this.dob;
    }

    /**
     * @param dob sets the dob of the employee.
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return return date of joining of the employee.
     */
    public String getDoj() {
        return this.doj;
    }

    /**
     * @param doj take date of joining of the employee.
     */
    public void setDoj(String doj) {
        this.doj = doj;
    }

    /**
     * @return return blood group of the employee.
     */
    public String getBloodGroup() {
        return this.bloodGroup;
    }

    /**
     * @param bloodGroup sets the blood group of the employee.
     */
    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    /**
     * @return returns employee's employment status.
     */
    public String getEmploymentStatus() {
        return this.employmentStatus;
    }

    /**
     * @param employmentStatus sets employee's employment status.
     */
    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    /**
     * @return returns designation of the employee.
     */
    public String getDesignation() {
        return this.designation;
    }

    /**
     * @param designation sets designation of the employee.
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * @return returns who has updated the profile.
     */
    public String getlastUpdateBy() {
        return this.lastUpdateBy;
    }

    /**
     * @param lastUpdateBy details updated by.
     */
    public void setlastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    /**
     * @return return uuid of the onw who has created the profile.
     */
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * @param createdBy takes the uuid of user who is creating profile.
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return return the time when the profile was created.
     */
    public String getCreatedOn() {
        return this.createdOn;
    }

    /**
     * @param createdOn sets the time when profile was created.
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the time and date when data was modified or updated.
     */
    public String getLastUpdatedOn() {
        return this.lastUpdatedOn;
    }

    /**
     * @param lastUpdatedOn date and time when the details were modified.
     */
    public void setLastUpdatedOn(String lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }
}
