/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * @author rahul sharma
 */
public class FamilyDetails {

    private String fullName = "";
    private String relation = "";
    private String dependent = "";
    private String dob = "";
    private String bloodGroup = "";

    private String lastUpdateBy = "";
    private String createdOn = "";
    private String createdBy = "";
    private String lastUpdatedOn = "";
    private String dependentID = "";

    /**
     * @return the time and date when data was modified or updated.
     */
    public String getLastUpdatedOn() {
        return this.lastUpdatedOn;
    }

    /**
     * @param lastUpdatedOn date and time when the details were modified.
     */
    public void setLastUpdatedOn(String lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    /**
     * @return return the time when the profile was created.
     */
    public String getCreatedOn() {
        return this.createdOn;
    }

    /**
     * @param createdOn sets the time when profile was created.
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return return the full name of the family member.
     */
    public String getFullName() {
        return this.fullName;
    }

    /**
     * @param fullName take family member's full name.
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return returns the relation between employee and family member.
     */
    public String getRelation() {
        return this.relation;
    }

    /**
     * @param relation take the relation b/w employee and family member as the
     *            input.
     */
    public void setRelation(String relation) {
        this.relation = relation;
    }

    /**
     * @return return dependent of the employee.
     */
    public String getDependent() {
        return this.dependent;
    }

    /**
     * @param dependent takes dependent as the input.
     */
    public void setDependent(String dependent) {
        this.dependent = dependent;
    }

    /**
     * @return return the date of birth of family member.
     */
    public String getDob() {
        return this.dob;
    }

    /**
     * @param dob taken the date of birth of the member.
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return return the blood group of the member.
     */
    public String getBloodGroup() {
        return this.bloodGroup;
    }

    /**
     * @param bloodGroup takes the blood group of member as input.
     */
    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    /**
     * @return returns who has updated the profile.
     */
    public String getlastUpdateBy() {
        return this.lastUpdateBy;
    }

    /**
     * @param lastUpdateBy details updated by.
     */
    public void setlastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    /**
     * @return returns the uuid of the user who created the profile.
     */
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * @param createdBy sets the uuid of the user who is creatingthe profile.
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return return the dependent Id.
     */
    public String getDependentID() {
        return this.dependentID;
    }

    /**
     * @param dependentID sets the id of the dependent.
     */
    public void setDependentID(String dependentID) {
        this.dependentID = dependentID;
    }

}
