/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

import java.util.ArrayList;

/**
 * Wrapper Class to be be used by SearchResultArray. value means the importance
 * w.r.t to the parameter.
 * @author skishore
 */
public class SearchResult {
    /**
     * @return value
     */
    public int getValue() {
        return value;
    }

    /**
     * @param value to be set
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * @return personal details
     */
    public PersonalDetailsObject getPersonalDetails() {
        return personalDetails;
    }

    /**
     * @param personalDetails to be returned
     */
    public void setPersonalDetails(PersonalDetailsObject personalDetails) {
        this.personalDetails = personalDetails;
    }

    /**
     * @return  address details
     */
    public ArrayList<AddressDetailsObject> getAdrressDetails() {
        return adrressDetails;
    }

    /**
     * @param adrressDetails to be set
     */
    public void setAdrressDetails(ArrayList<AddressDetailsObject> adrressDetails) {
        this.adrressDetails = adrressDetails;
    }

    /**
     * @return commiunication details to be returned
     */
    public ArrayList<CommunicationDetailsObject> getCommunicatiDetails() {
        return communicatiDetails;
    }

    /**
     * @param communicatiDetails to be set
     */
    public void setCommunicatiDetails(ArrayList<CommunicationDetailsObject> communicatiDetails) {
        this.communicatiDetails = communicatiDetails;
    }

    /**
     * @return family details to be returned
     */
    public ArrayList<FamilyDetailsObject> getFamilyDetails() {
        return familyDetails;
    }

    /**
     * @param familyDetails to be set
     */
    public void setFamilyDetails(ArrayList<FamilyDetailsObject> familyDetails) {
        this.familyDetails = familyDetails;
    }

    private int value = 1;
    private PersonalDetailsObject personalDetails;
    private ArrayList<AddressDetailsObject> adrressDetails;
    private ArrayList<CommunicationDetailsObject> communicatiDetails;
    private ArrayList<FamilyDetailsObject> familyDetails;
}
