/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

import java.util.ArrayList;

/**
 * Wrapper Class for to be returned for search query.
 * @author skishore
 */
public class SearchResultArray {
    /**
     * @return list of employee
     */
    public ArrayList<SearchResult> getEmployeeList() {
        return employeeList;
    }

    /**
     * @param employeeList is set
     */
    public void setEmployeeList(ArrayList<SearchResult> employeeList) {
        this.employeeList = employeeList;
    }

    private ArrayList<SearchResult> employeeList = new ArrayList<SearchResult>();
}
