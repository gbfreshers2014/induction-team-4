/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

import java.util.ArrayList;

import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * Tasks to be done.
 * @author skishore
 */
public class TaskObject extends AbstractResponse {
    private ArrayList<Taskdetails> task = new ArrayList<Taskdetails>();

    /**
     * @return task object
     */
    public ArrayList<Taskdetails> getTask() {
        return task;
    }

    /**
     * @param task to be set
     */
    public void setTask(ArrayList<Taskdetails> task) {
        this.task = task;
    }

}
