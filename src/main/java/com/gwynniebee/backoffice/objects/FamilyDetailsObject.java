/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

import java.sql.Date;

/**
 * Wrapper Class for Employee Communication Details.
 * @author skishore
 */
public class FamilyDetailsObject {

    /**
     * @return fullname of parent
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName is set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return depenednecy boolean
     */
    public boolean isDependent() {
        return dependent;
    }

    /**
     * @param dependent is set
     */
    public void setDependent(boolean dependent) {
        this.dependent = dependent;
    }

    /**
     * @return date of birth
     */
    public Date getDOB() {
        return dOB;
    }

    /**
     * @param dOB is set
     */
    public void setDOB(Date dOB) {
        this.dOB = dOB;
    }

    /**
     * @return blood group
     */
    public String getBloodGroup() {
        return bloodGroup;
    }

    /**
     * @param bloodGroup  is set
     */
    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    private String bloodGroup;
    private boolean dependent;
    private Date dOB;
    private String fullName;
}
