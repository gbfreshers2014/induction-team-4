/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * @author rahul sharma
 */
public class CommunicationDetails {
    // public CommunicationDetails() {
    // // TODO Auto-generated constructor stub
    // }

    private String lastUpdateBy = "";
    private String createdBy = "";
    private String createdOn = "";
    private String lastUpdatedOn = "";

    private String type = "";
    private String details = "";

    /**
     * @return the time and date when data was modified or updated.
     */
    public String getLastUpdatedOn() {
        return this.lastUpdatedOn;
    }

    /**
     * @param lastUpdatedOn date and time when the details were modified.
     */
    public void setLastUpdatedOn(String lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    /**
     * @return return the time when the profile was created.
     */
    public String getCreatedOn() {
        return this.createdOn;
    }

    /**
     * @param createdOn sets the time when profile was created.
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return return the type of communication details of the employee.
     */
    public String getType() {
        return this.type;
    }

    /**
     * @param type set the type of communication details of the employee.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return return the communication details.
     */
    public String getDetails() {
        return this.details;
    }

    /**
     * @param details set communication details of the employee.
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * @return returns who has updated the profile.
     */
    public String getLastUpdateBy() {
        return this.lastUpdateBy;
    }

    /**
     * @param lastUpdateBy details updated by.
     */
    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    /**
     * @return return uuid of the onw who has created the profile.
     */
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * @param createdBy takes the uuid of user who is creating profile.
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}
