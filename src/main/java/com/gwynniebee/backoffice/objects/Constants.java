/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * Constants class for testing.
 * @author skishore
 */
public final class Constants {

    private Constants() {
        // TODO Auto-generated constructor stub
    }

    /*
     * public static final String VERSION_ID = "version_id"; public static final
     * String USER_ID = "uuid"; public static final int EMAIL_MAX_LENGTH = 255;
     * public static final int UUID_MAX_LENGTH = 36; public static final int
     * MAX_ADDRESSES = 2; public static final String FIRST_NAME = "first_name";
     * public static final String LAST_NAME = "last_name"; public static final
     * String EMAIL = "emai_idl"; public static final String LAST_UPDATED_BY =
     * "last_updated_by"; public static final String LAST_UPDATED_ON =
     * "last_updated_on"; public static final String CREATED_ON = "created_on";
     * public static final String CREATED_BY = "created_by"; public static final
     * String EMPLOYMENT_STATUS = "emploeyement_status"; public static final
     * String EMPLOYEE_ID = "emp_id"; public static final String DATE_OF_BIRTH =
     * "dob"; public static final String DATE_OF_JOINING = "doj"; public static
     * final String EMPLOYEE_BLODD_GROUP = "emp_blood_group"; public static
     * final String EMPLOYEE_DESIGNATION = "designation"; public static final
     * String FAMILY_MEMBER_BLOOD_GROUP = "blood_group"; public static final
     * String FAMILY_MEMBER_NAME = "full_name"; public static final String
     * DEPENDENT_STATUS = "dependent"; public static final String
     * FAMILY_MEMBER_RELATION = "relation"; public static final String
     * COMMUNICTAION_TYPE = "type"; public static final String
     * COMMUNICATION_DETAILS = "details"; public static final String
     * ADDRESS_TYPE = "type"; public static final String STREET_ADDRESS =
     * "street_address"; public static final String ADDRESS_CITY = "city";
     * public static final String ADDRESS_STATE = "state"; public static final
     * String ADDRESS_COUNTRY = "country"; public static final String ZIP_CODE =
     * "zip_code"; public static final String ATTENDANCE_DATE = "date";
     */
    public static final String DATABASE_SOURCE_NAME = "jdbc/Employee-team2";
    public static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DATABASE_EMPLOYEE_URL = "jdbc:mysql://localhost/team4-employee-information-";
    public static final String DATABASE_USERNAME = "root";
    public static final String DATABASE_PASSWORD = "gwynniebee";
    public static final String ENTRYNAME = "gbfreshers";
    public static final String FILENAME = "attendance.csv";
    public static final int LOOKUP = 10;
    public static final String UUID = "UUID";
    public static final String SMALL_UUID = "uuid";
    // urls routing automatic starts from here.........
    // public static final String =""

}
