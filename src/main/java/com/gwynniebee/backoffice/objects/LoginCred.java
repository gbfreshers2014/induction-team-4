/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * @author rahul sharma
 */
public class LoginCred {

    private String password = "";
    private String role = "";

    private String lastUpdateBy = "";
    private String createdBy = "";

    /**
     * gets the password.
     * @return returns password.
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * sets password of the employee.
     * @param password password of the employee.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * gets the role of the employee.
     * @return returns the role of the employee.
     */
    public String getRole() {
        return this.role;
    }

    /**
     * @param role role of the user(admin or not).
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return returns who has updated the profile.
     */
    public String getLastUpdateBy() {
        return this.lastUpdateBy;
    }

    /**
     * @param lastUpdateBy details updated by.
     */
    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    /**
     * @return return uuid of the onw who has created the profile.
     */
    public String getCreatedBy() {
        return this.createdBy;
    }

    /**
     * @param createdBy takes the uuid of user who is creating profile.
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}
