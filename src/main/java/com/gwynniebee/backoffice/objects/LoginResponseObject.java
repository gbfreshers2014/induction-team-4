/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * @author rahul sharma
 */
public class LoginResponseObject {
    private String role;
    private String uuid;

    /**
     * @return return role of the user.
     */
    public String getRole() {
        return this.role;
    }

    /**
     * @param role takes role of the user.
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return return the uuid of the user.
     */
    public String getUUID() {
        return this.uuid;
    }

    /**
     * @param uuid takes uuid of the employee as input.
     */
    public void setUUID(String uuid) {
        this.uuid = uuid;
    }
}
