/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

import java.sql.Date;

/**
 * Wrapper Class used to return the attendance of the employee.
 * @author skishore
 */
public class LeavesDetails {
    /**
     * @return date of enquiry
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date is set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return status of that day
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status is set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    private Date date;
    private String status;
}
