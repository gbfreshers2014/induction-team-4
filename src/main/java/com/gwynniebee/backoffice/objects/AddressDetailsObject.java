/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * Wrapper class for Address Details.
 * @author skishore
 */
public class AddressDetailsObject {
    /**
     * @return street address
     */
    public String getStreetAddress() {
        return streetAddress;
    }

    /**
     * @param streetAddress is set
     */
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    /**
     * @return city of user
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city is set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return state of user
     */
    public String getState() {
        return state;
    }

    /**
     * @param state is set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return countrycode
     */
    public String getCountrycode() {
        return countrycode;
    }

    /**
     * @param countrycode is set
     */
    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    /**
     * @return the iipcode
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     * @param zipcode is set
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    private String city;
    private String countrycode;
    private String state;
    private String streetAddress;
    private String zipcode;

}
