/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * @author rahul sharma
 */
public class AddressDetails {
    private String type = "";
    private String streetAddress = "";
    private String city = "";
    private String state = "";
    private String country = "";
    private String zipcode = "";
    private String createdBy = "";
    private String lastUpdateBy = "";
    private String createdOn = "";
    private String lastUpdatedOn = "";

    /**
     * @return the time and date when data was modified or updated.
     */
    public String getLastUpdatedOn() {
        return this.lastUpdatedOn;
    }

    /**
     * @param lastUpdatedOn date and time when the details were modified.
     */
    public void setLastUpdatedOn(String lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }

    /**
     * @return return the time when the profile was created.
     */
    public String getCreatedOn() {
        return this.createdOn;
    }

    /**
     * @param createdOn sets the time when profile was created.
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return type of address.
     */
    public String getType() {
        return this.type;
    }

    /**
     * @param type set type of address.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return returns the street adress.
     */
    public String getStreetAddress() {
        return this.streetAddress;
    }

    /**
     * @param streetAddress takes the address of the employee
     */
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    /**
     * @return return city.
     */
    public String getCity() {
        return this.city;
    }

    /**
     * @param city takes the city where employee lives as the input.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return returns the state where employee lives.
     */
    public String getState() {
        return this.state;
    }

    /**
     * @param state takes state as the input.
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return return the country where employee lives.
     */
    public String getCountry() {
        return this.country;
    }

    /**
     * @param country where employee lives.
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return zipcode of the area.
     */
    public String getZipcode() {
        return this.zipcode;
    }

    /**
     * @param zipcode zipcode of the area.
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     * @return returns who has updated the profile.
     */
    public String getLastUpdateBy() {
        return this.lastUpdateBy;
    }

    /**
     * @param lastUpdateBy details updated by.
     */
    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    /**
     * @return returns the id of the user who has created the profile.
     */
    public String getCreatedBy() {
        return this.createdBy;

    }

    /**
     * @param createdBy takes the uuid of user who is creating profile.
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

}
