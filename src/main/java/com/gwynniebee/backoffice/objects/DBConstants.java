/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * DBConstants CLASSS.
 * @author skishore
 */
public final class DBConstants {

    private DBConstants() {

    }

    /**
     * @return bucket name
     */
    public static String getBucket() {
        return bucket;
    }

    /**
     * @param bucketname is set
     */
    public static void setBucket(String bucketname) {
        bucket = bucketname;
    }

    public static final String DATA_SOURCE_NAME = "jdbc/Team4";
    public static final String URL = "gwynniebee_users.url";
    public static final String USERNAME = "gwynniebee_users.username";
    public static final String PASSWORD = "gwynniebee_users.password";
    public static final String DRIVER = "gwynniebee_users.driver";
    private static String bucket;

    // public static final String DATA_SOURCE_NAME =
    // "jdbc:mysql://induction-dev-t4.gwynniebee.com/team4-employee-information";
    // public static final String USERNAME = "write_all_bi";
    // public static final String PASSWORD = "write_all_bi";
    // public static final String DRIVER = "com.mysql.jdbc.Driver";

}
