/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * Individual task detials.
 * @author skishore
 */
public class Taskdetails {
    /**
     * @return date
     */
    public java.sql.Date getDate() {
        return date;
    }
    /**
     * @param date is set
     */
    public void setDate(java.sql.Date date) {
        this.date = date;
    }
    /**
     * @return action
     */
    public String getAction() {
        return action;
    }
    /**
     * @param action is set
     */
    public void setAction(String action) {
        this.action = action;
    }
    /**
     * @return updating uuid
     */
    public String getCreatedby() {
        return createdby;
    }
    /**
     * @param createdby is set
     */
    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }
    private java.sql.Date date;
    private String action;
    private String createdby;

}
