/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * @author rahul sharma
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndividualLeaveObject {
    private String updatingUuid = "";
    private String date = "";
    private String attendanceStatus = "";
    private String uuid = "";

    /**
     * @return outputs the id of the user who is updaing the attendance of the
     *         individual.
     */
    public String getUpdatingUuid() {
        return this.updatingUuid;
    }

    /**
     * @param updatingUuid the id of the user who is updaing the attendance of
     *            the individual.
     */
    public void setUpdatingUuid(String updatingUuid) {
        this.updatingUuid = updatingUuid;
    }

    /**
     * @return return the date.
     */
    public String getDate() {
        return this.date;
    }

    /**
     * @param date returns the date.
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return return the status(present/absent)
     */
    public String getAttendanceStatus() {
        return this.attendanceStatus;
    }

    /**
     * @param attendanceStatus status
     */
    public void setAttendanceStatus(String attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }

    /**
     * @return uuid
     */
    public String getUuid() {
        return this.uuid;
    }

    /**
     * @param uuid id of the user.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
