/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * @author rahul sharma
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatusResponseObject {
    private int status;
    private String message = "";

    /**
     * @return returns the status.
     */
    public int getStatus() {
        return this.status;
    }

    /**
     * @param status set the status of the query.
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return return the message.
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * @param message sets the message.
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
