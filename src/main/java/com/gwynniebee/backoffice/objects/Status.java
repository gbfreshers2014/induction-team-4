/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * @author rahul
 */
public class Status {
    private int code;
    private String message;

    /**
     * @return returns code(1 or 0, depending upon the result of the query.
     */
    public int getCode() {
        return this.code;
    }

    /**
     * @param code sets the code value
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return outputs the response.
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * @param message sets the message.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
