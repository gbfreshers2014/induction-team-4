/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * Wrapper Class for Employee Personal Details.
 * @author skishore
 */
public class PersonalDetailsObject {
    /**
     * @return firstname
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName is set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return lastname
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName is set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return email id
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email is set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid is set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return bloodgrup
     */
    public String getBloodGroup() {
        return bloodGroup;
    }

    /**
     * @param bloodGroup is set
     */
    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    /**
     * @return retired or employeed
     */
    public String getEmploymentStatus() {
        return employmentStatus;
    }

    /**
     * @param employmentStatus is set
     */
    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    /**
     * @return designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * @param designation is set
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * @param uuid of employee
     * @param fname of employee
     * @param lname of employee
     * @param emaiID of employee
     */
    public PersonalDetailsObject(String uuid, String fname, String lname, String emaiID) {
        firstName = fname;
        lastName = lname;
        this.email = emaiID;
        this.uuid = uuid;
    }

    private String bloodGroup;
    private String designation;
    private String email;
    private String employmentStatus;
    private String firstName;
    private String lastName;
    private String uuid;

}
