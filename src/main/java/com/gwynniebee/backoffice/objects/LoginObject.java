/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * Wrapper Class for taking LOgin input (emailid,password) from end user.
 * @author skishore
 */
public class LoginObject {
    private String emailid;
    private String password;

    /**
     * @return email id
     */
    public String getEmailid() {
        return emailid;
    }

    /**
     * @param emailid is set
     */
    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    /**
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password is set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString() overridded
     */
    @Override
    public String toString() {

        return emailid + password;

    }
}
