/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rahul sharma
 */
public class EmployeeDetails {

    private PersonalDetails personalDetails = new PersonalDetails();
    private List<AddressDetails> addressDetails = new ArrayList<AddressDetails>();
    private List<CommunicationDetails> communicationDetails = new ArrayList<CommunicationDetails>();
    private List<FamilyDetails> familyDetails = new ArrayList<FamilyDetails>();

    /**
     * gets the personal details of the employee.
     * @return returns the personal details of the employee.
     */
    public PersonalDetails getPersonalDetails() {
        return this.personalDetails;
    }

    /**
     * set the personal details of the employee.
     * @param personalDetails contains the personal details of the employee.
     */
    public void setPersonalDetails(PersonalDetails personalDetails) {
        this.personalDetails = personalDetails;
    }

    /**
     * gets the address details of the employee.
     * @return return the list of address detials of the employee.
     */
    public List<AddressDetails> getAddressDetails() {
        return this.addressDetails;
    }

    /**
     * sets the address details of the employee.
     * @param addressDetails contains details of the address
     */
    public void setAddressDetails(List<AddressDetails> addressDetails) {
        this.addressDetails = addressDetails;
    }

    /**
     * gets the list of communication details of the employee.
     * @return returns the list of communication details of the employee.
     */
    public List<CommunicationDetails> getCommunicationDetails() {
        return this.communicationDetails;
    }

    /**
     * sets the communication details of the employee.
     * @param communicationDetails contains the communication details of
     *            employee.
     */
    public void setCommunicationDetails(List<CommunicationDetails> communicationDetails) {
        this.communicationDetails = communicationDetails;
    }

    /**
     * gets the list of family details of the employee.
     * @return returns the list of family details of employee.
     */
    public List<FamilyDetails> getFamilyDetails() {
        return this.familyDetails;
    }

    /**
     * sets the list of family details of the employee.
     * @param familyDetails sets the family details of the employee
     */
    public void setFamilyDetails(List<FamilyDetails> familyDetails) {
        this.familyDetails = familyDetails;
    }

}
