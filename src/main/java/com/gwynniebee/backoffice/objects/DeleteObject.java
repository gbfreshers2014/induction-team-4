/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.backoffice.objects;

/**
 * input from user in request.
 * @author skishore,vikas
 */
public class DeleteObject {

    /**
     * @return the updateduuid
     */
    public String getUpdatinguuid() {
        return updatinguuid;
    }

    /**
     * @param updatinguuid is set
     */
    public void setUpdatinguuid(String updatinguuid) {
        this.updatinguuid = updatinguuid;
    }

    private String uuid;
    private String updatinguuid;

    /**
     * @return UUID of user
     */
    public String getUUID() {
        return uuid;
    }

    /**
     * @param uuid is set
     */
    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

}
