#! /usr/bin/env python

import os
import re
import sys
import tempfile
from getopt import getopt, GetoptError

gb_home = "/home/gb"

def usage():
    """Prints out the usage"""
    print """
Usage: %s -c <property-file>

-c    rest template configuration file
""" % (sys.argv[0])

def get_options():
    """Parses command line options.

@return: **options
"""
    # Required options.
    required = ["c"]

    # Get command line options.
    try:
        option_string = ''.join(map(lambda x: x + ":", required))
        options, [] = getopt(sys.argv[1:], option_string)
    except GetoptError:
        usage()
        exit(1)

    # Convert options to dict and validate.
    result = dict()
    for option in options:
        (opt, value) = option
        opt = opt[1:]
        if (opt in required):
            result[opt] = value
        else:
            usage()
            exit(1)

    # Verify that all mandatory parameters are provided.
    for option in required:
        if not result.has_key(option):
            usage()
            exit(1)

    return result

def parse_config_file(filename):
    """Parse configuration file and return a hash"""
    result = dict()
    file = open(filename, "rb")
    while True:
        line = file.readline()
        if not line:
            break
        line = line.rstrip("\r\n")
        if line != "" and line[0] != "#":
            [key, value] = line.split("=")
            result[key] = value
    return result

def combine_war(war_file):
    """Builds a new war with configuration"""
    # Copy configuration file.
    tmpdir = tempfile.mkdtemp()
    os.mkdir(tmpdir + "/WEB-INF")
    classdir = tmpdir + "/WEB-INF/classes"
    os.mkdir(classdir)
    command = "cp %s/conf/employee_REST_API/emp_prop.properties %s" \
                % (gb_home, classdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "jar -uf %s -C %s WEB-INF/classes/emp_prop.properties" \
                % (war_file, tmpdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "cp %s/conf/employee_REST_API/log4j.properties %s" \
                % (gb_home, classdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "jar -uf %s -C %s WEB-INF/classes/log4j.properties" \
                % (war_file, tmpdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    lib_dir = tmpdir + "/WEB-INF/lib"
    os.mkdir(lib_dir)
    contextdir = tmpdir + "/META-INF"
    os.mkdir(contextdir)

    command = "cp %s/conf/employee_REST_API/context.xml %s" \
                % (gb_home, contextdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)

    command = "jar -uf %s -C %s META-INF/context.xml" \
                % (war_file, tmpdir)
    print "Executing: " + command
    result = os.system(command)
    if result != 0:
        print "Error: " + command + " failed"
        sys.exit(1)
    os.system("rm -rf " + tmpdir)
def generate_tomcat_context (config):
    driver_variable = "\$\{gwynniebee_users\.driver\}"
    url_variable = "\$\{gwynniebee_users\.url\}"
    username_variable = "\$\{gwynniebee_users\.username\}"
    password_variable = "\$\{gwynniebee_users\.password\}"

    context_driver = config["gwynniebee_users.driver"]
    context_url = config["gwynniebee_users.url"]
    context_username = config["gwynniebee_users.username"]
    context_password = config["gwynniebee_users.password"]

    file_name = gb_home + "/conf/employee_REST_API/context.xml"
    out_file_name = file_name + ".tmp"

    with open (file_name) as template:
        out = open (out_file_name, "w")
        for line in template:
            if (re.search(driver_variable, line)):
                out.write(re.sub(driver_variable, context_driver, line))
            elif (re.search(url_variable, line)):
                out.write(re.sub(url_variable, context_url, line))
            elif (re.search(username_variable, line)):
                out.write(re.sub(username_variable, context_username, line))
            elif (re.search(password_variable, line)):
                out.write(re.sub(password_variable, context_password, line))
            else:
                out.write(line)

        out.close()
        os.rename (out_file_name, file_name)

def main():
    """Main function"""
    options = get_options()
    config = parse_config_file(options["c"])

    server = config["application.server"]
    port = config["application.server.http.port"]
    uri = config["application.deployment.tomcat.manager.url"]
    login = config["application.deployment.tomcat.manager.login"]
    password = config["application.deployment.tomcat.manager.password"]
    context = config["application.context.path"]
    war_file = gb_home + "/lib/employee_REST_API/employee_REST_API*.war"
    tomcat_path = config["application.deployment.tomcat.app.dir"]
    generate_tomcat_context(config)
    combine_war(war_file)

    print "Installing war"
    request_uri = "http://%s:%s@%s:%s%s/deploy?path=%s&update=true" \
                    % (login, password, server, port, uri, context)
    curl_command = "OUTPUT=`/usr/bin/curl -s -w '%%{http_code}\n' -X PUT --upload-file %s '%s' 2>&1`;echo \"$OUTPUT\";if echo \"$OUTPUT\" | grep -q '^2[0-9][0-9]$';then if echo $OUTPUT | grep -q '^FAIL';then false;else true;fi;else false;fi" \
                    % (war_file, request_uri)
    print "Executing: " + curl_command
    result = os.system(curl_command)
    if result != 0:
        print "Error: " + curl_command + " failed"
        sys.exit(1)

    sys.exit(0)

if __name__ == "__main__":
    main()
